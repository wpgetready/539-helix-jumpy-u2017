echo off
echo INSTRUCCIONES: Este bat reconfigura el proyecto para Spinner Fall. Para compilar pasar el parametro -compile
echo Si se compila, el ejecutable queda en folder other. Revisar stdout.log tiene info importante del resultado.
echo on

set prjpath =%cd%
@echo %prjpath%
"D:\Program Files\Unity2017.4.40f1\Editor\Unity.exe" -quit -batchmode -logfile stdout.log  -projectPath "D:\ESPACIO-TRABAJO\20170502-KMFactory\PROD\539-helix-jumpy\20201030-balling-fall-v2" %prjpath% -executeMethod compiler.launch %1
