﻿Shader "Holistic P2/ch-51-outline-advanced" {
/*
Outline has 2 ways of making it:
1-The logical one, which you'll understand wihtout problems: 
We use 2 objects, 1 is our object and other is the same object which is bigger using vertex extruding.
The second object we make a single color and we turn the buffer off and thats is a outline , inefficient yes, but it works.

The second technique is a outline in 3D, not even the parrot would understand it, but it also works but differently because is 3d.
This is the second thecnique, which in theory is better but there are few things in 3d that maybe we don't want.
So, two techniques, 2 differents but very similar results. Your choice.
*/
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_OutlineColor ("Outline Color", Color) =(0,0,0,1)
		_Outline("Outline Width", Range (.002, 0.1)) = .005
	}
	SubShader {


		CGPROGRAM
			#pragma surface surf Lambert
			struct Input {
				float2 uv_MainTex;
			};
			sampler2D _MainTex;
			void surf (Input IN, inout SurfaceOutput o) {
				o.Albedo= tex2D (_MainTex,IN.uv_MainTex).rgb;
			}
		ENDCG

		Pass {
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex: POSITION;
				float3 normal: NORMAL;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				fixed4 color: COLOR;
			};

			float _Outline;
			float4 _OutlineColor;

			v2f vert(appdata v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);

				float3 norm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal ));
				float2 offset =TransformViewToProjection(norm.xy);

				o.pos.xy +=offset * o.pos.z * _Outline;
				o.color = _OutlineColor;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target 
			{
				return i.color;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
