﻿Shader "Holistic P2/ch-40-blending-2-images" {
	Properties {
			_MainTex   ("2D Texture"   , 2D) = "white" {}
			_DecalTex   ("2D Texture"   , 2D) = "white" {}
			[Toggle] _ShowDecal("Show Decal?", Float) =0 //What you should know is when is on it will have a value of 1.0
	}
	SubShader {
		Tags {	"Queue" ="Geometry"	}

		CGPROGRAM
			#pragma surface surf Lambert

			sampler2D _MainTex;
			sampler2D _DecalTex;
			float _ShowDecal;
			
			struct Input {	float2 uv_MainTex;	};

			void surf (Input IN, inout SurfaceOutput o) {
				fixed4 a = tex2D(_MainTex,IN.uv_MainTex);
				fixed4 b = tex2D(_DecalTex,IN.uv_MainTex) * _ShowDecal;
				o.Albedo   = b.r>0.9 ? b.rgb : a.rgb;
				//Another ideas from the questions:
				// https://www.udemy.com/course/unity-shaders/learn/lecture/8569532#questions/8051964
				//o.Albedo = lerp(a.rgb,b.rgb, b.a); However the effect isn't noticed maybe need a time constant to make it better.
				//Bumping the texture:
				//https://www.udemy.com/course/unity-shaders/learn/lecture/8569532#questions/4240060
			}
		ENDCG
	}
	FallBack "Diffuse"

}