20201007:
Todas las clases ScriptableObject tienen como misión crear archivos .assets.
Luego que cumplen su misión, ya no tienen uso. 
Se podrían borrar y no afectaría al juego.
Sin embargo, es buena idea mantenerlo si desea uno crear nuevos Scriptable Objects basados 
en un formato.
Los Assets tambien se pueden duplicar como se quiera y donde se quiera , configurarlos a gusto
y usarlos

Rectificación: NO debería borrarse la clase porque se usa como plantilla.
PERO si puede borrarse el menu que las genera porque en muchos casos no tiene sentido crear ninguna clase más de referencia y en el peor de los casos
se duplica algun asset creado y se trabaja sobre el duplicado.

20210405: Ahora bien, lo que nunca nombre es que hay clases especiales capaces de crear scriptable objects mediante codigo
Un buen ejemplo es la clase Level.cs capaz de crear un nivel y grabarlo en una carpeta.

Usando esta analogia, es posible guardar informacion de forma PERMANENTE de un juego.
De hecho, este problema me estaba persiguiendo.

Un buen ejemplo es este:
El juego lleva un tracking de los logros del jugador en la opcion Challenge.
Va grabando todos los datos, pero la realidad es que es una instancia que se borra cada vez que se inicia Unity.
En consecuencia, al empezar el juego de nuevo, se olvida completamente de los datos que habian antes, empezando de cero.
Esto es malo.

20210407: Se elimino la clase de tracking y se reemplazo por un sistema de persistencia.


