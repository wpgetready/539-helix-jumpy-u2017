﻿//The following structure stores data to be later retrieved to make a repeatable level

using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[System.Serializable]
public struct deadPiece
{
    public int piece; //between 0-11
    public bool isMovable;
    public bool oneDirection;
    public bool lerpMov;
}

[System.Serializable]
public struct DisabledPieceData{
    public List<int> pieces;
}

[System.Serializable]
public struct DeadPieceData
{
    public List<deadPiece> pieces;
}

[System.Serializable]
public struct PowerUpPosition
{
    public int HelixNumber;
    public int PieceNumber;
}

[System.Serializable]
public struct PowerUpData
{
    public List<PowerUpPosition> Position;
}

/*1.0.1: Improved: discard repeated elements
 1.0.2: reverted 1.0.1
 1.0.3 simplified structure 
 1.0.4 back to normal again: ScriptableObjects can't handle composed structures like List<int>[] (an array of list of int) directly, we need to use struct for that task.
 1.0.5: Scratched HelixController->createHelix method, making more intuitive , simple and straightforward. 
 This will improve the way of maintain and change a level.
 1.0.6: Added powerup structure
     */
[System.Serializable]
[CreateAssetMenu(fileName = "Level", menuName = "Game/Level", order = 7)]
public class Level: ScriptableObject
{
    public int levelNumber;
    public DisabledPieceData[] DisabledPieces; //This is an array of List of integers
    public DeadPieceData[] DeadPieces;
    public PowerUpData PowerUps;

    public void init(int howManyHelixes, int levelNumber)
    {
        this.levelNumber = levelNumber;
        DisabledPieces = new DisabledPieceData[howManyHelixes + 1];
        DeadPieces = new DeadPieceData[howManyHelixes + 1];
        
        //Initialize lists
        for (int i = 0; i < DisabledPieces.Length; i++)      { DisabledPieces[i].pieces = new List<int>(); }
        for (int i = 0; i < DeadPieces.Length; i++)        { DeadPieces[i].pieces = new List<deadPiece>(); }
        PowerUps.Position = new List<PowerUpPosition>();
    }

    public void insertDisabledPiece(int helixNumber, int pieceNumber)
    {
            DisabledPieces[helixNumber].pieces.Add(pieceNumber);
    }

    public void insertDeadPiece(int helixNumber, bool isMovable, bool oneDirection, bool lerpMov,int piece)
    {
        DeadPieces[helixNumber].pieces.Add(new deadPiece { piece = piece, isMovable = isMovable, oneDirection = oneDirection, lerpMov = lerpMov, });
    }

    public void insertPowerUp(int helixNumber, int helixPieceNumber)
    {
        PowerUps.Position.Add(new PowerUpPosition { HelixNumber = helixNumber, PieceNumber = helixPieceNumber });
    }

    public void saveAsset()
    {
#if UNITY_EDITOR
        string path = "Assets/Balling_Fall/Scripts/ScriptableObjects/Game1/Challenge/challenge_" + string.Format("{0,3:D3}", levelNumber) + ".asset";
        Debug.Log("Challenge Level " + this.levelNumber + " saved:" + path);
        AssetDatabase.CreateAsset(this, path);
#endif
    }
}