﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum GameMode { Challenge, Infinite }

[CreateAssetMenu(fileName = "GameplayConfig", menuName = "Game/Gameplay Config", order = 1)]
public class GameplayConfig : ScriptableObject {
    [Header("This settings are important for configuration and gameplaying")]
    public int testingLevel = 0;
    public float reviveWaitTime = 4f;
    public Vector3 firstHelixPosition = new Vector3(0, -1f, 2.5f);
    public float helixSpace = 5f;
    public float fadingHelixScale = 4f;
    public float fadingHelixTime = 0.5f;
    public int helixPassedCountForBreak = 2;
    public int threeStarPercentTime = 50;
    public int twoStarPercentTime = 30;
    public int oneStarPercentTime = 10;
    public float uIFadingTime = 1f;
    public float ballSplatFadingTime = 2f;
    public Material backgroundMaterial; //This material needs to be assigned to the Sky .Also, this material uses GradientSkyShader with some specific parameters.
                                        //We need additional documentation here!
    [Header("Backgrounds works as ranges. Example: define screen 1 - 20 with some color combination")]
    public ScriptableObject[] backgroundColors;
    [Header("Game Levels are defined as ranges, making series of increasing difficulty")]
    public ScriptableObject[] gameLevels;
    [Header("Particles are defined as ranges where particles are active.")]
    public ScriptableObject[] particles;
    [Header("Tracks if we are playing Challenge or Infinite mode")]
    public GameMode currentGameMode; //tracks what is the current mode the Player is playing: Challenge / Infinite.
                                     //Depending this value, the game will be using challengeTOR/infiniteTOR to read and register data.
/*
    [Header("Tracking data for Infinite Mode")]
    public playerRecord infiniteTOR; //Challenge Table of Records
    [Header("Tracking data for Challenge mode")]
    public playerRecord challengeTOR; //Challenge Table of Records
    */
    [Header("List of levels for Challenge mode")]
    public Level[] challengeLevels; //Levels generated for every Challenge.

    [Header("Compilation, keystore and ad configuration settings")]
    public AppConfig appConfig; //Game configuration: name , keystore, passwords, etc etc.

    [Header("Current Status for many buttons")]
    public bool btnSwipe; //current status true=swipe mode, false= two thumbs mode.
    public bool musicIsMuted; //true=music is on, false=music is off.
    public bool musicIsOff; //true=sound effects are on, othwerwise off.

}
