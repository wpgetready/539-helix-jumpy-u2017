﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelEnabler : MonoBehaviour {

	public bool Enable;
	public GameObject Panel;
	// Use this for initialization
	private bool _lastValue;
	void Start () {
		_lastValue = Enable;
		Panel.SetActive(_lastValue);
	}

    private void OnEnable()
    {
        _lastValue = Enable;
        Panel.SetActive(_lastValue);
    }

    // Update is called once per frame
    void Update () {
		if (_lastValue!=Enable) {
			_lastValue=Enable;
			Panel.SetActive(_lastValue);
		}
	}
}
