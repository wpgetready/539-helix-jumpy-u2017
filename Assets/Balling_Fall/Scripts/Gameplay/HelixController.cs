﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BallingFallGame;

public class HelixController : MonoBehaviour {

    [SerializeField] private HelixPieceController[] helixPieces;

    private bool movPieces = false;
    private bool randomValues = false;
    private bool oneDirection = false;
    private bool lerpMov = false;
    private int angleMov = 30;
    private float velMov = 1;
    private float velLerp = 0.2f;
    private float dstMeta = 1;
    private float percentMov = 20;


    public void CreateHelix(int disablePieces, int deadPieces, int currentLevel, Level lvl )
    {
        CreateHelix(disablePieces, deadPieces, currentLevel, lvl, new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
    }

    public void CreateHelix(int disablePieces, int deadPieces, int currentLevel, Level lvl, List<int> ListofPieces)
    {
        StartCoroutine(createHelix(disablePieces, deadPieces, currentLevel, lvl, ListofPieces));
    }



    /// <summary>
    /// Set disabled, dead and normal pieces.
    /// Simplified and (I hope) enhanced version
    /// </summary>
    /// <param name="disablePieces"></param>
    /// <param name="deadPieces"></param>
    /// <param name="nomalPieceColor"></param>
    /// <param name="deadPieceColor"></param>
    /// <param name="helixLevel"></param>
    /// <param name="lvl"></param>
    /// <returns></returns>
    private IEnumerator createHelix(int disablePieces, int deadPieces, int helixLevel, Level lvl, List<int>ListofPieces)
    {
        for (int i = 0; i < helixPieces.Length; i++)
        {
            helixPieces[i].SetNormalPiece();
        }

        bool isMovable = false;
        List<int> shuffled = shuffleList(ListofPieces); //get an unordered list of int from 0 to 11.
        for (int i = 0; i < disablePieces; i++)         //Disable all first elements on the list.
        {
            int disabledPiece = shuffled[i];
            lvl.insertDisabledPiece(helixLevel, disabledPiece);
            helixPieces[disabledPiece].Disable();
            yield return null;
        }
        
        for (int i = disablePieces; i < disablePieces + deadPieces; i++) //convert to deadPieces the following elements
        {
            int deadPiece = shuffled[i];
            if (movPieces)
            {
                isMovable = Random.Range(1, 100) <= percentMov ? true : false; //RANDOM - 3
                if (randomValues)
                {
                    oneDirection = Random.Range(0, 2) == 0 ? false : true; //RANDOM - 4
                    lerpMov = Random.Range(0, 2) == 0 ? false : true;       //RANDOM - 5
                }
                if (isMovable) helixPieces[deadPiece].SetDeadPieceWithMov(oneDirection, lerpMov, angleMov, velMov, velLerp, dstMeta);
                else helixPieces[deadPiece].SetMortalPiece();
            }
            else
            {
                helixPieces[deadPiece].SetMortalPiece();
            }
            lvl.insertDeadPiece(helixLevel, isMovable, oneDirection, lerpMov, deadPiece);
            yield return null;
        }
        //20201023: since  we set all pieces to initially normal, this loop is not longer needed.
        /*
        for (int i = disablePieces + deadPieces; i < shuffled.Count; i++) //Set rest of elements to normalpiece
        {
            int normalPiece = shuffled[i];
            helixPieces[normalPiece].SetNormalPiece();
        }
        */
    }


    public void CreateHelixPerlin(int startingPiece, int pieceRange, int currentLevel, Level lvl)
    {
        StartCoroutine(createHelixPerlin(startingPiece, pieceRange, currentLevel, lvl));
    }

    /// <summary>
    /// Makes a hole in an helix starting from some helix piece
    /// Simplified and (I hope) enhanced version
    /// </summary>
    /// <param name="startingPiece"></param>
    /// <param name="pieceRange"></param>
    /// <param name="nomalPieceColor"></param>
    /// <param name="deadPieceColor"></param>
    /// <param name="helixLevel"></param>
    /// <param name="lvl"></param>
    /// <returns></returns>
    private IEnumerator createHelixPerlin(int startingPiece, int pieceRange, int helixLevel, Level lvl)
    {
        //Insert values in list
        List<int> pieces = new List<int>();
        for (int i = startingPiece; i < startingPiece + pieceRange; i++)
        {
            pieces.Add(i % 12);
        }

        //The for is 0 to 11, so for should be smart enough.
        for (int i = 0; i < 12; i++) //Set rest of elements to normalpiece
        {
            if (pieces.Contains(i))
            {
                helixPieces[i].Disable();
                lvl.insertDisabledPiece(helixLevel, i);
            }
            else
            {
                helixPieces[i].SetNormalPiece();
            }
        }
        yield return null;
    }
        
    /// <summary>
    /// Regenerates the screen according the data provided for the level. New version, simpler and shorter.
    /// IMPORTANT: Since RegenHelix mimics CreateHelix behavior, it uses same parameters. However, almost all of them are useless(!).The only two used are currentLevel and lvl.
    /// </summary>
    /// <param name="disablePieces"></param>
    /// <param name="deadPieces"></param>
    /// <param name="nomalPieceColor"></param>
    /// <param name="deadPieceColor"></param>
    /// <param name="currentLevel"></param>
    /// <param name="lvl"></param>
    public void RegenHelix(int disablePieces, int deadPieces,  int currentLevel, Level lvl)
    {
        StartCoroutine(regenHelix(disablePieces, deadPieces, currentLevel, lvl));
    }

    private IEnumerator regenHelix(int disablePieces, int deadPieces, int helixLevel, Level lvl)
    {
        bool isMovable = false;
        List<int> piecesBag = new List<int>(); //This is list to keep tracking which pieces are not normal.
        List<int> disabledPiecesList = lvl.DisabledPieces[helixLevel].pieces; //get disabled pieces from level

        for (int i = 0; i < disabledPiecesList.Count; i++)         //Disable all first elements on the list.
        {
            int disabledPiece = disabledPiecesList[i];
            helixPieces[disabledPiece].Disable();
            piecesBag.Add(disabledPiece); //Add the disable piece to the bag
            yield return null;
        }

        List<deadPiece> deadPiecesList = lvl.DeadPieces[helixLevel].pieces;

        for (int i = 0; i < deadPiecesList.Count; i++)
        {
            deadPiece deadPiece = deadPiecesList[i];
            if (movPieces)
            {
                isMovable = deadPiece.isMovable;
                if (randomValues)
                {
                    oneDirection = deadPiece.oneDirection;
                    lerpMov = deadPiece.lerpMov;
                }
                if (isMovable) helixPieces[deadPiece.piece].SetDeadPieceWithMov(oneDirection, lerpMov, angleMov, velMov, velLerp, dstMeta);
                else helixPieces[deadPiece.piece].SetMortalPiece();
            }
            else
            {
                helixPieces[deadPiece.piece].SetMortalPiece();
            }
            piecesBag.Add(deadPiece.piece);//add the dead piece to the bag
            yield return null;
        }

        for (int i = 0; i < helixPieces.Length; i++)
        {
            if (!piecesBag.Contains(i))
            {
                helixPieces[i].SetNormalPiece();
            }
        }
    }
    
    /// <summary>
    /// Returns a shuffled list of integer between 0 and 11
    /// Based on ideas over https://stackoverflow.com/questions/273313/randomize-a-listt ,specially second one because its simplicity.
    /// </summary>
    /// <returns></returns>
    private List<int> shuffleList(List<int> list)
    {
        //List<int> Lint = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        return list.OrderBy(a => System.Guid.NewGuid()).ToList();
    }

       public void ShatterAllPieces()
    {
        foreach (HelixPieceController o in helixPieces)
        {
            o.Shatter();
        }
        GameManager.Instance.CreateFadingHelix(transform.position);
    }

    public void SetMovPiece(bool _randomValues, bool _oneDirection, bool _lerpMov, int _angleMov, float _vel, float _velLerp, float _dstMeta, int _percentMov)
    {
        movPieces = true;
        randomValues = _randomValues;
        oneDirection = _oneDirection;
        lerpMov = _lerpMov;
        angleMov = _angleMov;
        velMov = _vel;
        velLerp = _velLerp;
        dstMeta = _dstMeta;
        percentMov = _percentMov;
    }

    public static int countNormalPieces(HelixController hc) 
    {
        int counter = 0;
        HelixPieceController[] hpc = hc.GetComponentsInChildren<HelixPieceController>();
        foreach (HelixPieceController h in hpc)
        {
            GameObject g = h.gameObject;
            if (g.CompareTag("HelixPiece"))
            {
                //Check if mesh is disabled
                if (g.GetComponent<MeshRenderer>().enabled)
                {
                    counter++;
                }
            }
        }
        return counter;
    }
}