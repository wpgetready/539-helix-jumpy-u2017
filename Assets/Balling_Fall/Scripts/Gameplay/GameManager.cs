﻿using BallingFallGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;


//Several game states used along the game
public enum GameState { Prepare, Playing, Pause, Revive, PassLevel, GameOver,PassChallenge}

//Define behavior when creating special levels (what I called Perlin Mode).
public enum challengeStyle { turnToLeft, turnToRight, freeStyle }

public class GameManager : MonoBehaviour
{


    [Header("Gameplay Configuration")]
    [SerializeField] public GameplayConfig GPC;

    [Header("Materials and Object needed for the game")]
    //Materials needed for the entire game
    [SerializeField] private Material pillarMaterial;
    [SerializeField] public Material deadPieceMaterial;
    [SerializeField] public Material normalPieceMaterial;
    [SerializeField] public Material brokenPieceMaterial;
    [SerializeField] public Material powerUpMaterial; //material to assign the player and trail when powerup is in effect.

    //Objects needed to construct the Helix
    //Unity BUG: if you put [SerializeField] private readonly GameObject X (see the READONLY), the interface HIDES the variable AND FROM THAT IN AHEAD.
    //ALSO IT WILL ERASE THE VALUES FROM THAT, so be VERY careful of defining [SerializeField] private readonly...is DANGEROUS!
    [SerializeField] private Transform rotaterTrans;
    [SerializeField] private GameObject pillar;
    [SerializeField] private GameObject bottomPillar;
    [SerializeField] private GameObject helixPrefab;
    [SerializeField] private GameObject fadingHelixPrefab;
    [SerializeField] private GameObject ballSplatPrefab;
    [SerializeField] private GameObject splatShatterPrefab;
    [SerializeField] private GameObject powerUpPrefab;

    public static GameManager Instance { private set; get; }
    public static event System.Action<GameState> GameStateChanged = delegate { };
    public static int CurrentLevel { private set; get; }
    public static bool IsRestart { private set; get; }

    //Tracking data for current mode (Challenge/Infinite)
    public playerRecord trackData;
    public int Hits; //Used to define if a player made a perfect play (player didn't touch ANY piece on any level).
    
    //Several settings for the game
    public float ReviveWaitTime { private set; get; }
    public int PassedCountForBreakHelix { private set; get; }
    public int TimeToPassLevel { private set; get; }
    public int ThreeStarTime { private set; get; }
    public int TwoStarTime { private set; get; }
    public int OneStarTime { private set; get; }
    public bool IsFinishedFading { private set; get; }
    public bool IsRevived { private set; get; }
	private static System.DateTime startTime;
	public static int elapsedTime; //time in miliseconds for passing the level.

    private GameState gameState = GameState.GameOver;
    private List<BallSplatController> listBallSplatControl = new List<BallSplatController>();
    private List<FadingHelixController> listFadingHelixControl = new List<FadingHelixController>();
    private List<ParticleSystem> listSplatShatter = new List<ParticleSystem>();

    //PlayerController send messages to several classes, including GameManager.
    //PlayerStateChanged needs to be active in order to get these messages.
    private void OnEnable()
    {
        PlayerController.PlayerStateChanged += PlayerController_PlayerStateChanged;
    }
    private void OnDisable()
    {
        PlayerController.PlayerStateChanged -= PlayerController_PlayerStateChanged;
    }
    private void PlayerController_PlayerStateChanged(PlayerState obj)
    {
        switch (obj)
        {
            //Prepare,Living and Pause are not handled here
            case PlayerState.PassLevel:
                StartCoroutine(PassLevel()); //we pass the level
                break;
            case PlayerState.Die:
                trackElapsedTime(false); //track time when the player dies(totalTimePlayed),but do not track totalTime
                if (IsRevived) //If it is already revived go to game over. This is done only one time 
                               // The idea behind is Reviving shows ads, but revive several times
                               //will flow the player with several ads, which I do not recommend.
                {
                    GameOver();
                    return;
                }
                //Let's check if there is a video ready to be displayed. If it fails, game over
                if (!AdManager.Instance.IsRewardedVideoAdReady())
                {
                    GameOver();
                    return;
                }
                //Otherwise run the revive process.We are going to try, because if player cancel video, it loose the life.
                TryRevive();
                break;
            default:
                break;
        }
    }

    public void restartGame()
    {

        markLevelCompleted(CurrentLevel);
        trackData.SaveJson();

        if (GPC.testingLevel != 0)
        {
            UIManager.resetScore();
        }

        //This line triggers an event on this class. This event is captured by AdmobClass.
        //20191009: currently is configured so when there are 3 restarts , an ad will be displayed.
        //This is exactly how Helix Jump (the original) fires ads.
        PlayList.Instance.InitMusic(0.5f);

        GameState = GameState.Prepare; //Launch Prepare event. WATCH OUT. Prepare can't be launched in the Load Scene. Order in events is relevant!

        LoadScene(SceneManager.GetActiveScene().name, 0.5f);
        UIManager.resetScore();
    }

    /// <summary>
    /// Launch the game. If there is a testingLevel, the score is reset.
    /// </summary>
    public void nextGame()
    {
        GameState = GameState.Prepare; //Launch event exactly before starting the game.
                                       //20210714: if we are in Challenge Mode and pass level 100, we finish the game.
        if (GPC.currentGameMode == GameMode.Challenge && CurrentLevel == 100)
        {
            //Challenge Mode is finished, activate win screen
            GameState = GameState.PassChallenge;
            //Reset music and start over. Wait for menu button.
            PlayList.Instance.RestartMusic();

        }
        else
        {
            setNextLevel(CurrentLevel);
            LoadScene(SceneManager.GetActiveScene().name, 0.5f);
        }
    }

    public GameState GameState
    {
        get
        {
            return gameState;
        }
        private set
        {
            if (value == gameState) return;
            gameState = value;
            if (GameState == null) return;
            GameStateChanged(gameState);
        }
    }

    /// <summary>
    /// Only available when Challenge is completed (100 levels) and menu available 
    /// </summary>
    public void backMainMenu()
    {
        PlayList.Instance.PauseMusic();
        LoadScene("Levels", 1.0f);
    }

    private void Awake()
    {
        if (Instance != null)  DestroyImmediate(Instance.gameObject);
        Instance = this;
    }

    private void OnDestroy()
    {
        if (Instance == this) Instance = null;
    }
    
    private void Start()
    {
        //Try to get the tracking data from file.
        trackData = new playerRecord(GPC.currentGameMode);
        trackData.LoadJson();
        Hits = 0;  //Track perfect play (how many times the player touches the floor)
        Application.targetFrameRate = 60;

        IsRevived = false;
        Time.timeScale = 1f; //Reset time if there was some error when making slow motion...

        //20210410: With the new detection method, ResetPassedCount has no meaning. In the future, will be removed.
        HelixDetector.ResetPassedCount();//Reset any counter related to the player.

        CurrentLevel = getCurrentLevel();
        if (GPC.testingLevel != 0) CurrentLevel = GPC.testingLevel;

        UIManager.Instance.SetLevelTxt(CurrentLevel);  //Show level on UI
        UIManager.Instance.updateHighScore(trackData.bestScore);

        //Every level has some particles related. The particles changes depending current level.
        //If a level is in a specific range, it will have a specific background.
        //For example level 1-5 bkg 1, level 6-10 bkg 2, etc.
        //Look for ScriptableObjects/Game1/Particles
        foreach (ParticlesConfig ps in GPC.particles)
        {
            if (CurrentLevel >= ps.MinLevel && CurrentLevel <= ps.MaxLevel)
            {
                CreateParticles(ps);
            }
        }

        //The background works in similar fashion as particles. 
        //To avoid excesive repetition and some variation, the background changes every some levels.
        //Look for ScriptableObjects/Games1/Levels
        foreach (BackgroundConfig b in GPC.backgroundColors)
        {
            if (CurrentLevel >= b.MinLevel && CurrentLevel <= b.MaxLevel)
            {
                CreateBackground(GPC.backgroundMaterial, b);
                break;
            }
        }

        //Create level
        //In challenge Mode, every level was 'pre-recorded' in ScriptableObjects/Game1/Challenge
        //The levels can be changed manually or prerecorded from infinite levels.
        //(Se more about it in the games's documentation)
        foreach (GameLevelData o in GPC.gameLevels)
        {
            if (CurrentLevel >= o.MinLevel && CurrentLevel <= o.MaxLevel)
            {
                switch (GPC.currentGameMode) //Check current configuration
                {
                    case GameMode.Challenge: //Mode Challenge: regenerate level (load from asset)
                        Level lvl = GPC.challengeLevels[CurrentLevel]; //This list cannot be empty
                        if (lvl == null)
                        {
                            Debug.LogError("ERROR! level in " + CurrentLevel + " is empty!");
                        }
                        RegenLevel(o, lvl);
                        break;
                    case GameMode.Infinite: //Mode Infinite: create level random-based
                        //Also, decide if the level will be saved to later use for Challenge configuration.
	                    //Watchout! Save mode =true will erase any level with same number in challenge mode!
                        //Added Perlin Mode every 5 levels.
	                    if ((CurrentLevel % 5 == 0) && (CurrentLevel!=0)) {
                            if ((CurrentLevel % 10 == 0))
                            {
                                CreateLevelPerlin(o, false); //used for saving perlin levels in challenge mode.
                            }
                            else
                            {
                                CreateLevelPerlin(o, false);
                            }
	                    } else {
		                    CreateLevel(o, false);    
	                    }                    
                        break;
                    default:
                        break;
                }
                break;
            }
        }

        StartCoroutine(ResetIsFinishedFadingValue());
        UIManager.Instance.FadeOutPanel(GPC.uIFadingTime); //Fade the panel and start the game.
        if (IsRestart)
        {
            PlayingGame();
        }

       
    }

    /// <summary>
    /// Get current level. This will change depending if we are playing Challenge or Infinite Mode.
    /// </summary>
    /// <returns></returns>
    public int getCurrentLevel()
    {
        return trackData.currentLevel;
    }

    /// <summary>
    /// Actual start the game
    /// </summary>
    public void PlayingGame()
    {
        GameState = GameState.Playing;         //Fire event
    }

    /// <summary>
    /// Pause the game. Who{s is using this method?
    /// </summary>
    public void PauseGame()
    {
        GameState = GameState.Pause; //Fire event
    }

    /// <summary>
    /// Call Revive event
    /// </summary>
    public void TryRevive()
    {
        GameState = GameState.Revive; //Fire event
        Time.timeScale = 0.3f;
        StartCoroutine(slowMotion(1f, 7f)); //go to slow motion for 2 seconds
    }

    public void coroutineSlowMotion()
    {
        StartCoroutine(slowMotion(1f, 7f));
    }

    /*
*https://www.reddit.com/r/Unity3D/comments/4k1g3j/gradually_go_from_slow_motion_to_normal_speed/
Trying going from slow motion to normal speed to give user a chance of recovering after seeing video reward
*/
    private IEnumerator slowMotion(float _lerpTimeTo, float _timeToTake)
    {
        float endTime = Time.time + _timeToTake;
        float startTimeScale = Time.timeScale;
        float i = 0f;
        while (Time.time < endTime)
        {
            i += (1 / _timeToTake) * Time.deltaTime;
            Time.timeScale = Mathf.Lerp(startTimeScale, _lerpTimeTo, i);
            yield return null;
        }
        Time.timeScale = _lerpTimeTo;
    }


    #region PerfectLevel : Reward player when a perfect screen is made (no touched pieces, only in Perlin Mode).
    //A better score should be giving different score according how many touches made in the screen (0 -100k 1-50k 2-20k -3 10k 4-0

    /// <summary>
    /// Call PassLevel event
    /// </summary>
    public IEnumerator PassLevel()
    {
        //Get the time for this level.
        trackElapsedTime(true);
        PlayList.Instance.PauseMusic();
        SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.passLevel);
        UIManager.Instance.PerfectLevel(Hits); //Gives point in case of lower hits.
        yield return new WaitForSeconds(1.0f);
        ShareManager.Instance.CreateScreenshot();
        ShareManager.Instance.CreateScreenshotWithPrefix();
        GameState = GameState.PassLevel; //also fire event
        IsRestart = true;
    }


    /// <summary>
    /// Tracks the time for the player.
    /// This method is called from PassLevel and Die Level. If we pass the level, time is tracked in totalTime, but if the player dies, the time is ignored.
    /// only totalTimePlayed is added.
    /// </summary>
    /// <param name="playerPassLevel">if player pass level then totalTime is tracked. Otherwise, only totalTimePlayed is tracked</param>
    private void trackElapsedTime(bool playerPassLevel)
    {
        System.DateTime endTime = System.DateTime.Now;
        elapsedTime= (endTime - startTime).Seconds * 1000 + (endTime - startTime).Milliseconds;
        if (playerPassLevel) trackData.totalTime += elapsedTime;
        trackData.totalTimePlayed += elapsedTime;
        trackData.SaveJson();
    }
    #endregion


    private void setNextLevel(int currentLevel)
    {
        markLevelCompleted(currentLevel);
        trackData.currentLevel = currentLevel + 1;
        trackData.SaveJson();
        CurrentLevel++;        
    }

    private void setCurrentLevel(int currentLevel)
    {
        if (GPC.testingLevel!=0)
        {
            CurrentLevel = GPC.testingLevel;
            return;
        }
        trackData.currentLevel = currentLevel;
    }

    /// <summary>
    /// Marks the level as completed
    /// IMPORTANT: This method DOES NOT SAVE the updates.
    /// That's because every method calling markLevelCompleted is respnsible of saving when the time is appropiate.
    /// </summary>
    /// <param name="currentLevel"></param>
    private void markLevelCompleted(int currentLevel)
    {
        int idx = trackData.trackingLevel.FindIndex(x => x.level == currentLevel);
        if (idx != -1)
        {
            //Nothing todo yet, we know we already have a tracking
            //The data should be replaced if some conditions are met, por example
            //1-bestScore is surpassed,2-bestStars surpassed, 3-besTime is surpased, 4-increase how many times we played it
            //TODO: Analyze those criteria before replacing. Maybe a merge of best results?
            //elapsedTime contains the time from the current game.
            levelCompleteData item = trackData.trackingLevel[idx];
            item.bestScore = trackData.trackingLevel[idx].bestScore;
            if (elapsedTime<item.bestTime)
            {
                item.bestTime = elapsedTime;
                item.bestTimeDate = System.DateTime.Now.ToString();
            } else
            {
                item.bestTime = trackData.trackingLevel[idx].bestTime;
            }
            
            item.deaths = trackData.trackingLevel[idx].deaths;
            item.level = trackData.trackingLevel[idx].level;
            item.played = trackData.trackingLevel[idx].played+1;
            trackData.trackingLevel[idx] = item;
        }
        else
        {
            trackData.trackingLevel.Add(new levelCompleteData { level = currentLevel, bestScore = 0, bestTime = elapsedTime, played = 0, deaths = 0,bestTimeDate ="" });
        }
        //trackData.SaveJson(); //persist on disk.
    }

    /// <summary>
    /// Call GameOver event
    /// </summary>
    public void GameOver()
    {
        GameState = GameState.GameOver; //Fire event.It will afect AdManager & UIManager
        IncreaseDeath(CurrentLevel);//increase number of death for this player.
        ShareManager.Instance.CreateScreenshot();
        ShareManager.Instance.CreateScreenshotWithPrefix();
        IsRestart = true;
        CurrentLevel = getCurrentLevel();
        PlayList.Instance.StopMusic();
    }

    private void IncreaseDeath(int currentLevel)
    {
        trackData.deathCounter += 1;
        int idx = trackData.trackingLevel.FindIndex(x => x.level == currentLevel);
        if (idx != -1)
        {
            levelCompleteData item = new levelCompleteData();
            item.bestScore = trackData.trackingLevel[idx].bestScore;
            item.bestTime = trackData.trackingLevel[idx].bestTime;
            item.deaths = trackData.trackingLevel[idx].deaths + 1;
            item.level =  trackData.trackingLevel[idx].level;
            item.played =  trackData.trackingLevel[idx].played+1;
            trackData.trackingLevel[idx] = item;
            Debug.Log("UPDATED DEATH");
        }
        trackData.SaveJson();
    }

    public void LoadScene(string sceneName, float delay)
    {
        StartCoroutine(LoadingScene(sceneName, delay));
    }

    private IEnumerator LoadingScene(string sceneName, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// Check if the Fading ended.
    /// IMPORTANT: while the fading is in progress, the player cannot play.
    /// That is controlled in RotatorController.cs
    /// Also, the time is measured from the right moment the player is able to play, not before.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ResetIsFinishedFadingValue()
    {
        IsFinishedFading = false;
        yield return new WaitForSeconds(GPC.uIFadingTime);
        IsFinishedFading = true;
        //20210412: the time is measured from the moment the player can play
	    startTime = System.DateTime.Now;
	    Debug.Log("Start Time" + startTime);
    }

    private IEnumerator PlayParticle(ParticleSystem par)
    {
        par.Play();
        yield return new WaitForSeconds(par.main.startLifetimeMultiplier);
        par.gameObject.SetActive(false);
    }

    //Get an inactive fading helix
    private FadingHelixController GetFadingHelixControl()
    {
        //Find on the list
        foreach (FadingHelixController o in listFadingHelixControl)
        {
            if (!o.gameObject.activeInHierarchy)
            {
                return o;
            }
        }

        //Didn't find one -> create new one
        FadingHelixController fadingHelixControl = Instantiate(fadingHelixPrefab, Vector3.zero, Quaternion.identity).GetComponent<FadingHelixController>();
        listFadingHelixControl.Add(fadingHelixControl);
        fadingHelixControl.gameObject.SetActive(false);
        return fadingHelixControl;
    }

    //Get an inactive ballSplatControl
    private BallSplatController GetBallSplatControl()
    {
        //Find on the list
        foreach (BallSplatController o in listBallSplatControl)
        {
            if (!o.gameObject.activeInHierarchy)
            {
                return o;
            }
        }

        //Didn't find one -> create new one
        BallSplatController ballSplatControl = Instantiate(ballSplatPrefab, Vector3.zero, Quaternion.identity).GetComponent<BallSplatController>();
        listBallSplatControl.Add(ballSplatControl);
        ballSplatControl.gameObject.SetActive(false);
        return ballSplatControl;
    }

    //Get an inactive splatShatter
    private ParticleSystem GetSplatShatter()
    {
        //Find on the list
        foreach (ParticleSystem o in listSplatShatter)
        {
            if (!o.gameObject.activeInHierarchy)
            {
                return o;
            }
        }

        //Didn't find one -> create new one
        ParticleSystem splatShatter = Instantiate(splatShatterPrefab, Vector3.zero, Quaternion.identity).GetComponent<ParticleSystem>();
        listSplatShatter.Add(splatShatter);
        splatShatter.gameObject.SetActive(false);
        return splatShatter;
    }

    //Instantiate a Prefab which it has a ParticleConfig and activates it.
    private void CreateParticles(ParticlesConfig pc)
    {
        ParticleSystem go = Instantiate<ParticleSystem>(pc.particleSystem, Camera.main.transform);
        go.gameObject.SetActive(true);
    }

        /// <summary>
        /// Assign colors to background. The material is a Shader Material with parameters and it needs to be
        /// assigned to the background, otherwise it won't work
        /// </summary>
        /// <param name="Mat"></param>
        /// <param name="b"></param>
    private void CreateBackground(Material Mat, BackgroundConfig b)
    {
        Mat.SetColor("_SkyColor1", b.TopColor);
        Mat.SetFloat("_SkyExponent1", b.TopExponent);
        Mat.SetColor("_SkyColor2", b.HorizonColor);
        Mat.SetColor("_SkyColor3", b.BottomColor);
        Mat.SetFloat("_SkyExponent2", b.BottomExponent);
        Mat.SetFloat("_SkyIntensity", b.SkyIntensity);
    }

    /**
     * Create the level using the data from levelData
     * 20101010: Included a class (Level) capable of registering all the data to regenerate it later.
     * 20201011: Corrected mathematical typo when generating range. 
     * Some refactoring was added to better readibility
     * 20201012: Correct way of instantiating SO
     * //https://answers.unity.com/questions/310847/how-to-create-instance-of-scriptableobject-and-pas.html
     * */

    private void CreateLevel(GameLevelData levelData, bool saveLevel)
    {
        //define difficulty level between 0 and 1. Example: if level is between 6 to 10 the difficulty would be [0,0.25,0.5,0.75,1] increasing on every level.
        float difficulty = (1.0f / (levelData.MaxLevel - levelData.MinLevel)) * (CurrentLevel - levelData.MinLevel);

        //Set how manys helixes this level would have. It is also increasing according difficulty
        int helixNumber = Mathf.RoundToInt(Mathf.Lerp(levelData.MinHelixNumber, levelData.MaxHelixNumber, difficulty));

        Level lvl = ScriptableObject.CreateInstance("Level") as Level;
        lvl.init(helixNumber, CurrentLevel); 

        //Assign colors to pieces
        deadPieceMaterial.color = levelData.DeadPieceColor;
        normalPieceMaterial.color = levelData.NormalPieceColor;
        brokenPieceMaterial.color = levelData.BrokenPieceColor;
        PlayerController.Instance.SetBallColor(levelData.BallColor);
        pillarMaterial.color = levelData.PillarColor;

        //Calculate time needed to pass,(yes it also depends on difficulty)
        TimeToPassLevel = Mathf.RoundToInt(Mathf.Lerp(levelData.MinTimeToPassLevel, levelData.MaxTimeToPassLevel, difficulty));

        //Create the first helix (an entire disc with 12 pieces , positioned always in the same place)
        HelixController firstHelixControl = Instantiate(helixPrefab, GPC.firstHelixPosition, Quaternion.identity).GetComponent<HelixController>();

        int disablePieces1 = Random.Range(levelData.MinDisablePiecesNumber, levelData.MaxDeadPiecesNumber + 1);
        //IMPORTANT: The player is over pieces 9 and 10. Create a platform where pieces 9 and 10 has to be placed, randomize the rest.
        firstHelixControl.CreateHelix(disablePieces1, 0, 0, lvl, new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 11 });
        //Assign to the parent to make sure it rotates along the pillar.
        firstHelixControl.transform.SetParent(rotaterTrans);

        //Calculate the height of all helixes, space and distance between the pillar and the first helix
        float oneHelixHeight = helixPrefab.transform.GetChild(0).GetComponent<Renderer>().bounds.size.y;
        float totalHelixHeight = oneHelixHeight * helixNumber - (helixNumber - 1) * oneHelixHeight + GPC.helixSpace;
        float totalSpace = GPC.helixSpace * (helixNumber - 1);
        float distance = Vector3.Distance(GPC.firstHelixPosition + Vector3.up * oneHelixHeight, pillar.transform.position);

        //Calculate and set the pillar's height
        float pillarHeight = totalSpace + totalHelixHeight + Mathf.Round(distance);
        pillar.transform.localScale = new Vector3(1, pillarHeight, 1);

        //Create helixes for the entire level
        Vector3 nextHelixPos = GPC.firstHelixPosition + Vector3.down * GPC.helixSpace;
        for (int i = 0; i < helixNumber - 1; i++)
        {
            HelixController helixControl = Instantiate(helixPrefab, nextHelixPos, Quaternion.identity).GetComponent<HelixController>();

            //Set data for moving pieces
            if (levelData.MovPieces)
            {
                helixControl.SetMovPiece(levelData.RandomValues, levelData.OneDirection, levelData.LerpMov, levelData.angleMov,
                                                               levelData.velMov, levelData.velLerp, levelData.dstMeta, levelData.percentMov);
            }

            int disablePieces = Random.Range(levelData.MinDisablePiecesNumber, levelData.MaxDisablePiecesNumber + 1);
            int deadPieces = Mathf.RoundToInt(Mathf.Lerp(levelData.MinDeadPiecesNumber, levelData.MaxDeadPiecesNumber, difficulty));
            helixControl.CreateHelix(disablePieces, deadPieces, i + 1, lvl);
            helixControl.transform.SetParent(rotaterTrans);
            nextHelixPos = helixControl.transform.position + Vector3.down * GPC.helixSpace;
        }

        //Move bottomHelix object to the bottom
        bottomPillar.transform.position = nextHelixPos + Vector3.up * oneHelixHeight; ;


        //Give some time to calculate all the objects, since we are dealing with coroutines.
        //There is ONE issue: this method does not save Powerups position, even the saveAsset tries to do that.
        //The problem is saving levels and generating Powerups are BOTH asynchronic and none of them will wait for the other.
        //So if we are not saving the level, we need to recreate de powerup generation.
        if (saveLevel)
        {
            StartCoroutine(wait1sec(lvl));
        }
        else
        {
            StartCoroutine(generatePowerup(lvl));
        }
    }

    /// <summary>
    ///   Save levels to use in the challenge mode.  Do NOT enable if you don't know what are you doing!!!
    /// </summary>
    /// <param name="lvl"></param>
    /// <returns></returns>    
    private IEnumerator wait1sec(Level lvl)
    {
        yield return new WaitForSeconds(1.0f); //give some time to process all the helixes ( asynchronic process)
        lvl.saveAsset();
        StartCoroutine(generatePowerup(lvl));
    }

    private IEnumerator generatePowerup(Level lvl)
    {
        yield return new WaitForSeconds(1.0f);
        powerUpSetup(lvl); //Define where are the powerups
    }

    private void CreateLevelPerlin(GameLevelData levelData, bool saveLevel)
    {
        CreateLevelPerlin(levelData, saveLevel, challengeStyle.freeStyle, 4); //4 is acceptable, 3 is hard, 2 is very hard, 1 is impossible.
    }

        /// <summary>
        /// Creates a new variety of levels that would be included every N levels.
        /// Ideally the idea is to introduce every 5 or 10 levels with some degree of difficulty.
        /// </summary>
        /// <param name="levelData"></param>
        /// <param name="saveLevel"></param>
        /// <param name="style"></param>
        /// <param name="HoleRange"></param>
    private void CreateLevelPerlin(GameLevelData levelData, bool saveLevel, challengeStyle style, int HoleRange)
    {
        float delta = 0.0f;
        //define difficulty level between 0 and 1.
        //For example, if the level is between 6 to 10 the difficulty would be [0,0.25,0.5,0.75,1] making the difficulty increasing in every level.
        float difficulty = (1.0f / (levelData.MaxLevel - levelData.MinLevel)) * (CurrentLevel - levelData.MinLevel);

        //Set how manys helixes this level would have. It is also increasing according difficulty
        int helixNumber = Mathf.RoundToInt(Mathf.Lerp(levelData.MinHelixNumber, levelData.MaxHelixNumber, difficulty));

        //Experimental: this class registers all data needed to create and recreate a level.
        //Level lvl = new Level(helixNumber,CurrentLevel); INCORRECT CALL, ScriptableObjects needs to be instantiated using CreateInstance
        Level lvl = ScriptableObject.CreateInstance("Level") as Level;
        lvl.init(helixNumber, CurrentLevel);

        //Assign colors to pieces
        deadPieceMaterial.color = levelData.DeadPieceColor;
        normalPieceMaterial.color = levelData.NormalPieceColor;
        brokenPieceMaterial.color = levelData.BrokenPieceColor;
        PlayerController.Instance.SetBallColor(levelData.BallColor);
        pillarMaterial.color = levelData.PillarColor;

        //Calculate time needed to pass,(yes it also depends on difficulty)
        TimeToPassLevel = Mathf.RoundToInt(Mathf.Lerp(levelData.MinTimeToPassLevel, levelData.MaxTimeToPassLevel, difficulty));

        //Create the first helix (put an entire disc with 12 pieces , positioned always in the same place)
        HelixController firstHelixControl = Instantiate(helixPrefab, GPC.firstHelixPosition, Quaternion.identity).GetComponent<HelixController>();

        //Generate first level without mortal pieces, as a first safety platform to start with the level (second parameter zero means 'No dead pieces'
        //To improve: there is a chance there is no floor under the player (1 against 12) so we should consider this
        //HOWEVER the player is positioned between two pieces, so the chance of no floor is 12^12 =1 against 144.

        //Ok, one issue here: we cannot select randomly any value, for example if we select 10 and we have to dissapear 3 blocks, the ball will start in the air.
        //So I will reduce the random range to avoid this case if possible.
        //Important: the ball is positioned above pieces 9 and 10. Se we cannot generate a case with 9 and 10 empty.
        int startingPiece = Random.Range(0, 7); //This should generate 0..6 , with 3 pieces would be [6,7,8] and 4 pieces [6,7,8,9] in worst case scenario.(we ignore position 11).
        //In every case , the ball would never start in the air (if we have more empty pieces, we should correct this).

        //Suggestions: for freeStyle use 4, for left or right use range 3
        int pieceRange = HoleRange; //it means well make a hole of piece range wide.

        firstHelixControl.CreateHelixPerlin(startingPiece, pieceRange, 0, lvl);
        firstHelixControl.transform.SetParent(rotaterTrans);

        //Calculate the height of all helixs, space and distance between the pillar and the first helix
        float oneHelixHeight = helixPrefab.transform.GetChild(0).GetComponent<Renderer>().bounds.size.y;
        float totalHelixHeight = oneHelixHeight * helixNumber - (helixNumber - 1) * oneHelixHeight + GPC.helixSpace;
        float totalSpace = GPC.helixSpace * (helixNumber - 1);
        float distance = Vector3.Distance(GPC.firstHelixPosition + Vector3.up * oneHelixHeight, pillar.transform.position);

        //Calculate and set the pillar's height
        float pillarHeight = totalSpace + totalHelixHeight + Mathf.Round(distance);
        pillar.transform.localScale = new Vector3(1, pillarHeight, 1);

        //Create helixs for the entire level
        Vector3 nextHelixPos = GPC.firstHelixPosition + Vector3.down * GPC.helixSpace;
        int dir = 2;
        int directionCount = 3;
        int[] opt1 = { 0, 2 };//if we are rotating to one direction, disallow abrupt rotation change
        int[] opt2 = { 1, 2 };//same
        for (int i = 0; i < helixNumber - 1; i++)
        {
            HelixController helixControl = Instantiate(helixPrefab, nextHelixPos, Quaternion.identity).GetComponent<HelixController>();

            //Set data for moving pieces
            //Perlin level do not have moving pieces. It is a special level for player to  get maximum points. It is a funny level!
            //if (levelData.MovPieces) helixControl.SetMovPiece(levelData.RandomValues, levelData.OneDirection, levelData.LerpMov, levelData.angleMov,
            //                                                   levelData.velMov, levelData.velLerp, levelData.dstMeta, levelData.percentMov);

            int disablePieces = Random.Range(levelData.MinDisablePiecesNumber, levelData.MaxDisablePiecesNumber + 1);
            //   int deadPieces = Mathf.RoundToInt(Mathf.Lerp(levelData.MinDeadPiecesNumber, levelData.MaxDeadPiecesNumber, difficulty));
            helixControl.CreateHelixPerlin(startingPiece, pieceRange, i + 1, lvl);
            helixControl.transform.SetParent(rotaterTrans);
            nextHelixPos = helixControl.transform.position + Vector3.down * GPC.helixSpace;

            switch (style)
            {
                case challengeStyle.turnToLeft:
                    startingPiece = Mathf.RoundToInt(startingPiece + Mathf.PerlinNoise(delta, 0) * 2);
                    startingPiece = startingPiece % 12;
                    delta += 0.01f; //When smaller, more continuois
                    break;
                case challengeStyle.turnToRight:
                    startingPiece = Mathf.RoundToInt(startingPiece - Mathf.PerlinNoise(delta, 0) * 2);
                    if (startingPiece < 0)
                    {
                        startingPiece = 11;
                    }
                    delta += 0.01f; //When smaller, more continuos
                    break;
                case challengeStyle.freeStyle:
                    //Here we calculate how to turn, to right (0)  or to left(1) or center(2). But it has to be continuous, otherwise is very hard.
                    //Since we need gradual changes, if I'm in the right (0) , I could stay the same (0) or turn to center(2) cada every x moves (currently 8)
                    //This is array opt1 [0,2]
                    //If I am in center(0) I could change to left (1) or right(2) . This is opt2[1,2]
                    //Finally, if I'm in te left(1) I can remain in the left(1) or center (0) . This is opt3[0,1]. SO the default seems to be wrong, (because
                    //I put (0..2) but in case of two I introduce a little of caos for few iterations (4).

                    if (directionCount <= 0)
                    {
                        //only allow smooth transitions. If we have zero we cannot allow 1. if we have 1, we cannot allow 0
                        switch (dir)
                        {
                            case 0:
                                dir = opt1[Random.Range(0, 2)]; //Select randomly 0 or 2
                                directionCount = 8;
                                if (dir == 2) directionCount = 4; //If result it is center, make it shorter so we have some more variation to right & left.
                                break;
                            case 1:
                                dir = opt2[Random.Range(0, 2)]; //Select randomly 1 or 2
                                directionCount = 8;
                                if (dir == 2) directionCount = 4;
                                break;
                            default: //This is the value 2
                                dir = Random.Range(0, 3); //Select Randomly a value between 0,1 or 2
                                directionCount = 8; //change the direction every 8 levels
                                if (dir == 2) directionCount = 4; //except when there is no change, change every 5 levels.
                                break;
                        }
                    }
                    else
                    {
                        switch (dir)
                        {
                            case 0:
                                startingPiece = Mathf.RoundToInt(startingPiece + Mathf.PerlinNoise(delta, 0) * 2);
                                startingPiece = startingPiece % 12;
                                delta += 0.01f; //When smaller, more continuois
                                break;
                            case 1:
                                startingPiece = Mathf.RoundToInt(startingPiece - Mathf.PerlinNoise(delta, 0) * 2);
                                if (startingPiece < 0)
                                {
                                    startingPiece = 11;
                                }
                                delta += 0.01f; //When smaller, more continuois
                                break;
                            default: //if 2 no direction change
                                break;
                        }
                        directionCount--;
                    }
                    break;
                default:
                    break;
            }
        }

        //Move bottomHelix object to the bottom
        bottomPillar.transform.position = nextHelixPos + Vector3.up * oneHelixHeight; ;

        //Finally, call the powerUpSetup, and place the powerup over the helix pieces.
        // powerUpSetup(lvl, saveLevel); //Define where are the powerups

        //Give some time to calculate all the objects, since we are dealing with coroutines.
        if (saveLevel)
        {
            StartCoroutine(wait1sec(lvl));
        }
    }

    //20201018:Currently, a powerup is active around 3.5 +- 1.0 s can reach 19-25 helix levels.
    //Based on that , I would recommend one powerup for every 25 levels of playing.
    //Currently, that implies there will be powerup from screen 6 and ahead.
    //A question: should it be always a powerup per level? That is another interesting question.
    //Notice: powerup will be defined by level configuration, and will be stored per-challenge configuration also, meaning it would be easy to reconfigure in case we need it.
    //20210409: I dropped the complex way of putting powerups. Now it is a helix number , a position between 0-11 and that's it. 
    ///Now it is very easy to find a powerup and foresee where it will be.

    /// <summary>
    /// This method is independent of level creation, in order to work independently.
    /// </summary>
    public void powerUpSetup(Level lvl)
    {
        //Start defining how many powerUps should be in this pillar. Find the RotatorController
        RotatorController rc = GameObject.FindObjectOfType<RotatorController>();
        HelixController[] hc = rc.GetComponentsInChildren<HelixController>();
        //Define where we gonna put the powerups. Return a list of helices where we going to put the powerups.
        List<int> puHelixes = PowerUp.definePowerUpLevels(hc.Length);
        //Loop around the levels
        for (int i = 0; i < puHelixes.Count; i++)
        {
            //Find out how many normal pieces there is in the helix
            HelixController currentHelix = hc[puHelixes[i]];
            int normalPiecesCount = HelixController.countNormalPieces(currentHelix);
            //Create a random number between 0 and the number of pieces.
            int piece = UnityEngine.Random.Range(0, normalPiecesCount); //Notice that if we have 3 pieces, it will return a number between 0-2
                                                                        //Also notice Range return a float and this is converted to integer.
            Debug.Log(normalPiecesCount + "piezas normales en total,POWER UP in level " + puHelixes[i] + ", piece " + piece);
            //Record this data in the piece position.
            //PROBLEM: Retrieving this is utterly complex, so I will devise a simpler way.

            //Create the powerup prefab in the center of the current selected Helix
            GameObject powerUp = Instantiate(powerUpPrefab, currentHelix.transform.position, Quaternion.identity);

            //The helix is a composition of objects. For that reason, we cannot get it size directly.
            //Instead I will use Bottompillar as a helper to get an approximate position for the powerup.
            //I'm going to get both ratio and put about middle of the difference of both  which is pilar + (botom-pilar)/2
            //Then I'm going to invert the result to position the powerup in the exact start position. 
            //Finally I'm going to rotate -18 degreess to make sure the powerup is over the starting HelixPiece.
            float pillarRatio = pillar.GetComponent<MeshRenderer>().bounds.size.x / 2;
            float bottomPillarRatio = bottomPillar.GetComponent<MeshRenderer>().bounds.size.x / 2;

            //Recall the object pivot was translated slightly up in order to match the proper y height. (using a composed gameobject,see the prefab).
            //Also the scale for the powerup was updated to be bigger and match the current game object sizes.
            //This decision helps to avoid positioning the powerup vertically, since it is already vertically positioned.

            //Translate the powerup to the starting position.
            powerUp.transform.Translate(-pillarRatio - (bottomPillarRatio - pillarRatio) / 2, 0, 0);
            int absolutePiecePosition = positionPowerUpOverHelixPiece(currentHelix, powerUp, piece);
            lvl.insertPowerUp(puHelixes[i], absolutePiecePosition); //Save powerup level data.
            //Put the object with parent the pillar, otherwise the powerup won't rotate around the pillar.
            powerUp.transform.SetParent(rotaterTrans);
        }
    }

    /*
     * RegenLevel is CreateLevel using a structure as reference.
     * It also uses RegenHelix instead CreateHelix to do so.
     * The Level data is used to reconstruct the helixes.
     * One IMPORTANT limitation: lvl.levelNumber y CurrentLevel has to match.
     * */

    private void RegenLevel(GameLevelData levelData, Level lvl)
    {
        //define difficulty level between 0 and 1.
        //For example, if the level is between 6 to 10 the difficulty would be [0,0.25,0.5,0.75,1] making the difficulty increasing in every level.
        float difficulty = (1.0f / (levelData.MaxLevel - levelData.MinLevel)) * (lvl.levelNumber - levelData.MinLevel);

        //Set how many levels this level would have. It is also increasing according difficulty
        int helixNumber = lvl.DisabledPieces.Length - 1;
        //20201024: This caused a lot of troubles. The level 21 had 1 level less due some calculation and it was giving us an error.
        //int helixNumber = Mathf.RoundToInt(Mathf.Lerp(levelData.MinHelixNumber, levelData.MaxHelixNumber, difficulty));

        //Assign colors to pieces
        deadPieceMaterial.color = levelData.DeadPieceColor;
        normalPieceMaterial.color = levelData.NormalPieceColor;
        brokenPieceMaterial.color = levelData.BrokenPieceColor;
        PlayerController.Instance.SetBallColor(levelData.BallColor);
        pillarMaterial.color = levelData.PillarColor;

        //Calculate time needed to pass,(yes it also depends on difficulty)
        TimeToPassLevel = Mathf.RoundToInt(Mathf.Lerp(levelData.MinTimeToPassLevel, levelData.MaxTimeToPassLevel, difficulty));

        //Create the first helix (put an entire disc with 12 pieces , positioned always in the same place)
        HelixController firstHelixControl = Instantiate(helixPrefab, GPC.firstHelixPosition, Quaternion.identity).GetComponent<HelixController>();

        //Generate first level without mortal pieces, as a first safety platform to start with the level (second parameter zero means 'No dead pieces'
        //To improve: there is a chance there is no floor under the player (1 against 12) so we should consider this
        //HOWEVER the player is positioned between two pieces, so the chance of no floor is 12^12 =1 against 144.
        //int disablePieces1 = Random.Range(levelData.MinDisablePiecesNumber, levelData.MaxDeadPiecesNumber + 1);

        //Sure we should pass this parameters, but we only mimick the working of CreateHelix as much as possible.
        //Define how many piece we'll going to disable it
        int disablePieces1 = lvl.DisabledPieces[0].pieces.Count;

        firstHelixControl.RegenHelix(disablePieces1, 0, 0, lvl);
        firstHelixControl.transform.SetParent(rotaterTrans);

        //Calculate the height of all helixs, space and distance between the pillar and the first helix
        float oneHelixHeight = helixPrefab.transform.GetChild(0).GetComponent<Renderer>().bounds.size.y;
        float totalHelixHeight = oneHelixHeight * helixNumber - (helixNumber - 1) * oneHelixHeight + GPC.helixSpace;
        float totalSpace = GPC.helixSpace * (helixNumber - 1);
        float distance = Vector3.Distance(GPC.firstHelixPosition + Vector3.up * oneHelixHeight, pillar.transform.position);

        //Calculate and set the pillar's height
        float pillarHeight = totalSpace + totalHelixHeight + Mathf.Round(distance);
        pillar.transform.localScale = new Vector3(1, pillarHeight, 1);

        //Create helices for the entire level
        Vector3 nextHelixPos = GPC.firstHelixPosition + Vector3.down * GPC.helixSpace;
        for (int i = 1; i < helixNumber; i++)
        {
            HelixController helixControl = Instantiate(helixPrefab, nextHelixPos, Quaternion.identity).GetComponent<HelixController>();

            //Set data for moving pieces
            if (levelData.MovPieces)
            {
                helixControl.SetMovPiece(levelData.RandomValues, levelData.OneDirection, levelData.LerpMov, levelData.angleMov,
                                                               levelData.velMov, levelData.velLerp, levelData.dstMeta, levelData.percentMov);
            }

            try
            {
                int disablePieces = lvl.DisabledPieces[i].pieces.Count;
                int deadPieces = Mathf.RoundToInt(Mathf.Lerp(levelData.MinDeadPiecesNumber, levelData.MaxDeadPiecesNumber, difficulty));
                helixControl.RegenHelix(disablePieces, deadPieces, i, lvl);
            }
            catch (System.Exception ex)
            {
                Debug.Log("Error: " + ex.Message + " i:" + i);
                throw ex;
            }

            helixControl.transform.SetParent(rotaterTrans);
            nextHelixPos = helixControl.transform.position + Vector3.down * GPC.helixSpace;
        }

        //Move bottomHelix object to the bottom
        bottomPillar.transform.position = nextHelixPos + Vector3.up * oneHelixHeight; ;
        StartCoroutine(createRegenPowerUp(lvl));
    }

    private IEnumerator createRegenPowerUp(Level lvl)
    {
        yield return new WaitForSeconds(2.0f);
        RegenPowerUpSetup(lvl);
    }

    /// <summary>
    /// Regenerate current PowerUp position based on level configuration
    /// This is the non procedural version.
    /// Yes I know: I should consolidate both in one, yes I know.
    /// But that implies the data should be in the same format, which is not.
    /// </summary>

    public void RegenPowerUpSetup(Level lvl)
    {
        //Start defining how many powerUps should be in this pillar. Find the RotatorController
        RotatorController rc = GameObject.FindObjectOfType<RotatorController>();
        HelixController[] hc = rc.GetComponentsInChildren<HelixController>();
        //Loop around the levels
        for (int i = 0; i < lvl.PowerUps.Position.Count; i++)
        {
            //Find out how many normal pieces there is in the helix
            HelixController currentHelix = hc[lvl.PowerUps.Position[i].HelixNumber];
            //Create the powerup prefab in the center of the current selected Helix
            GameObject powerUp = Instantiate(powerUpPrefab, currentHelix.transform.position, Quaternion.identity);

            Debug.Log("POWER UP in level " + lvl.PowerUps.Position[i].HelixNumber + ", piece " + lvl.PowerUps.Position[i].PieceNumber);


            //The helix is a composition of objects. For that reason, we cannot get it size directly.
            //Instead I will use Bottompillar as a helper to get an approximate position for the powerup.
            //I'm going to get both ratio and put about middle of the difference of both  which is pilar + (botom-pilar)/2
            //Then I'm going to invert the result to position the powerup in the exact start position. 
            //Finally I'm going to rotate -18 degreess to make sure the powerup is over the starting HelixPiece.
            //Notice also the position is rotating following clock's needle (horarly)
            float pillarRatio = pillar.GetComponent<MeshRenderer>().bounds.size.x / 2;
            float bottomPillarRatio = bottomPillar.GetComponent<MeshRenderer>().bounds.size.x / 2;

            //Recall the object pivot was translated slightly up in order to match the proper y height. (using a composed gameobject,see the prefab).
            //Also the scale for the powerup was updated to be bigger and match the current game object sizes.
            //This decision helps to avoid positioning the powerup vertically, since it is already vertically positioned.

            //This is positioning in the starting point.
            powerUp.transform.Translate(-pillarRatio - (bottomPillarRatio - pillarRatio) / 2, 0, 0);
            //Pass the currentHelix, the powerUp in the starting position, and over which piece the powerup should be.
            positionPowerUpOverHelixPiece(currentHelix, powerUp, lvl.PowerUps.Position[i].PieceNumber);
            //powerUp.transform.RotateAroundPoint(pillar.transform.position, Vector3.up, -18.0f + piece * 30.0f);
            //Lastly we put the object with parent the pillar, otherwise the powerup won't rotate around the pillar.
            powerUp.transform.SetParent(rotaterTrans);
        }
    }

    /// <summary>
    /// 20210404: After some tests, I found this is unnecesary complex, and leading to impredectible results.
    /// It is a LOT wiser if we follow this simple rule:
    /// JUST PUT THE POWERUP IN THE CORRESPONDENT PLACE, WITHOUT CHECKING ANYTHING ELSE.
    /// </summary>
    /// </summary>
    /// <param name="hc"></param>
    /// <param name="powerUp"></param>
    /// <param name="normalPieceNumber"></param>
    /// 
    private int positionPowerUpOverHelixPiece(HelixController hc, GameObject powerUp, int normalPieceNumber)
    {
        powerUp.transform.RotateAroundPoint(pillar.transform.position, Vector3.up, -18.0f + normalPieceNumber * 30.0f);
       // Debug.Log("Power UP in position:" + (normalPieceNumber * 30 - 18).ToString());
        return normalPieceNumber;
    }
        
    /// <summary>
    /// Continue the game
    /// </summary>
    public void SetContinueGame()
    {
        IsRevived = true;
        //GameManager.Instance.TimeToPassLevel
        PlayingGame();
    }

    /// <summary>
    /// Create a fading helix object at given position
    /// </summary>
    /// <param name="pos"></param>
    public void CreateFadingHelix(Vector3 pos)
    {
        FadingHelixController fadingHelixControl = GetFadingHelixControl();
        fadingHelixControl.transform.position = pos;
        fadingHelixControl.gameObject.SetActive(true);
        fadingHelixControl.FadingHelix(brokenPieceMaterial.color, GPC.fadingHelixScale, GPC.fadingHelixTime);
    }

    /// <summary>
    /// Create a ballSplat object at given position
    /// </summary>
    /// <param name="pos"></param>
    public void CreateBallSplat(Vector3 pos, Color playerColor, Transform parent)
    {
        BallSplatController ballSplatControl = GetBallSplatControl();
        ballSplatControl.transform.position = pos;
        ballSplatControl.transform.eulerAngles = new Vector3(90, Random.Range(0f, 360f), 0);
        ballSplatControl.gameObject.SetActive(true);
        ballSplatControl.FadeOut(playerColor, GPC.ballSplatFadingTime);
        ballSplatControl.transform.SetParent(parent);
    }

    /// <summary>
    /// Play splatShatter at given position
    /// </summary>
    /// <param name="pos"></param>
    public void PlaySplatShatter(Vector3 pos)
    {
        ParticleSystem splatShatter = GetSplatShatter();
        splatShatter.transform.position = pos;
        splatShatter.gameObject.SetActive(true);
        StartCoroutine(PlayParticle(splatShatter));
    }
}

//Rotate around is obsolete, so this extension should provide a solution according to
//https://answers.unity.com/questions/1698671/rotate-around-obsolete-missing-rotate-overloads.html
public static class TransformExt
{
    public static void RotateAroundPoint(this Transform aTrans, Vector3 aPoint, Vector3 aAxis, float aAngle)
    {
        Vector3 dir = aTrans.position - aPoint;
        Quaternion q = Quaternion.AngleAxis(aAngle, aAxis);
        aTrans.position = aPoint + q * dir;
        aTrans.rotation = q * aTrans.rotation;
    }
}

/*
20210407: This class had 1138 lines. It is very big and it needs to be improved somehow. A simplification is a must.
Additional documentation to grow up to 1163 lines...
20210408: Growth to more than 1250 lines. Now simplified and shrinked to 1066 , even now the documentation has been improved.
20210410: Shrinked to 1052 , even documentation grew a lot.
 * */

/*20210409:
 * The old fashion way of putting powerupos:
 *             //Before everything, how do we are going to put the powerups?
        //This is the main idea:
        //Return a list of helices where we going to put the powerups. (usually 1 or 2 powerups for all the level).
        //-Select the helix where we going to put the powerup
        //-Count how many normal pieces are in that helix (will return a number between 0-11)
        //Let's say we got a 5. It means that this helix has 5 normal pieces (the others are disabled/invisible)
        //-Create a random number between that count)...5) . Let's say we got a 3.
        //The idea is put the powerup in the third normal piece in the helix, to avoid putting in an empty space
        //and also in a mortal piece.
        //So we'll need a final function with some intelligence, that start counting every piece until 3 and put the powerup there.
        //It is important knowing that this normal pieces usually are not together, normally are distributed. 
        //So the function needs to be carefult to put the powerup in the proper place.
        //that intelligence is found in positionPowerUpOverHelixPiece

 positionPowerUpOverHelixPiece: OBSOLETE
     /// <summary>
    /// 20210404: Improved.
    /// The idea is return the REAL position where the powerup is put, instead the relative position based on 
    /// the positions of normal pieces. In this way, recreating the position is very fast if later we use this
    /// to save to a challenge.
    /// </summary>
    /// <param name="hc">The Helix to be analized</param>
    /// <param name="powerUp">PowerUp gameobject</param>
    /// <param name="normalPieceNumber">Normal Piece number where we want to put the powerup.</param>
    /// <returns></returns>
    private int positionPowerUpOverHelixPiece_OLD(HelixController hc, GameObject powerUp, int normalPieceNumber)
    {
        int pieceCounter = 0;
        //We need to count every normal piece to define where the poweUp will be.
        HelixPieceController[] hpc = hc.GetComponentsInChildren<HelixPieceController>();
        foreach (HelixPieceController h in hpc)
        {
            //A normal piece is a piece with tag HelixPiece and MeshRenderer enabled.
            GameObject g = h.gameObject;
            if (g.CompareTag("HelixPiece"))
            {
                //Check if mesh is disabled
                if (g.GetComponent<MeshRenderer>().enabled)
                {
                    normalPieceNumber--;
                    if (normalPieceNumber <= 0)
                    {
                        //Rotate powerup over the normal piece.
                        //We pass the reference object to rotate to (the pillar), the rotation vector (that should be the pilar axis)
                        //and finally how much degrees. This should be 360/12 =30 -18 which is the value to center the powerup
                        //inside the piece. (12 if the number of pieces in the helix)
                        powerUp.transform.RotateAroundPoint(pillar.transform.position, Vector3.up, -18.0f + pieceCounter * 30.0f);
                        break;
                    }
                }
            }
            pieceCounter++;
        }
        return pieceCounter; //Return the absolute piece position. This can be used later to save powerups position and to retrieve easily
    }
     */
