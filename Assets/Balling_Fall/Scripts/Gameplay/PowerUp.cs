﻿using BallingFallGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Documentation is at the bottom of this class
public enum powerUpState { on,off};

public class PowerUp : MonoBehaviour {
/*
    public static PowerUp Instance { private set; get; }

    void Awake()
    {
        if (Instance!= null) DestroyImmediate(Instance.gameObject);
        Instance = this;
    }
    
    void OnDestroy()
    {
        if (Instance == this) Instance = null;
    }
    */

    //Invulnerable variable marks if the player is invulnerable.It's a flag saying when the effect starts and stops. 
    //This solves some scenarios (prevent touching the powerup and inmediately die after
    //hitting the ground, and avoid dying immediately after the powerup is gone)

    public static Boolean Invulnerable = false;
	public static event Action<powerUpState> PowerUpEvent = delegate { };

    private const float TRANSPARENT_ALPHA = 0.0f;
	private const float EFFECT_TIME = 3.5f ; //In seconds
	private float FADEOUT_TIME= 0.5f ; //In seconds
	
    //The detail of defining static is because there is ONE instance of this variables, which prevents issues handling multiples powerups stats.
	private static float waitUntil;
	private static float timeWait=EFFECT_TIME;
	
	public static powerUpState currentPU = powerUpState.off;

    public powerUpState powerupEvent
    {
        get { return currentPU; }
        private set
	    {
		    if (value == currentPU) return;
            currentPU = value;
            if (PowerUpEvent != null) PowerUpEvent(currentPU);
        }
    }

    private void OnEnable() { PlayerController.PlayerStateChanged += PlayerController_PlayerStateChanged;   }
    private void OnDisable() { PlayerController.PlayerStateChanged -= PlayerController_PlayerStateChanged;    }

	//Remember to disable the powerup if the player pass the level or dies.
    private void PlayerController_PlayerStateChanged(PlayerState obj)
    {
        switch (obj)
        {
			case PlayerState.PassLevel:
				powerupEvent = powerUpState.off;
				Invulnerable=false;
                break;
            case PlayerState.Die:
	            powerupEvent = powerUpState.off;
	            Invulnerable=false;
                break;
            default:
                break;
        }
    }

	//What happens if the player touch the powerup?
    private void OnTriggerEnter(Collider other)
    {
	    if (!other.CompareTag("Player")) return; //ignore any collision except the player

	    Invulnerable=true; //the player cannot die when powerup is touched
	    //Notice that powerup takes a little moment to be turned on.
	    
        SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.powerUpContact);//Play contact audio when powerup is touched.
        gameObject.GetComponent<MeshCollider>().isTrigger = false; //Disable the chance the player hit the object several times.

		//This effect makes the powerup invisible
	    //Even though is hardly noticeable (because it quickly dissapear from screen)
	    StartCoroutine(MakeItInvisible(TRANSPARENT_ALPHA, FADEOUT_TIME)); 
        
	    //Decided the time should be predictable, I get ride of random part.
	    //timeWait = powerUpEffectTime + UnityEngine.Random.Range(0.0f, powerUpEffectTimeMaxRandom);
	    timeWait = EFFECT_TIME; //timeWait is used in FixedUpdate event, check it.
        
	    //Start powerupevent only if we have the power off. If it is on , don't start it again.
	    if (powerupEvent== powerUpState.on) return;
        StartCoroutine(StartPowerUp());
    }

    /*
     * Make the object progresively invisible
     * In order to work, the object has to have a material supporting Transparency
     * for more info: https://stackoverflow.com/questions/51522721/unity-c-sharp-change-opacity-of-the-material
     * */
    IEnumerator MakeItInvisible(float FadeTo,float TimeToFade)
	{
		var MR =gameObject.GetComponent<MeshRenderer>();
        Color CC = MR.material.color;
	    for (float t = 0.0f;  t < TimeToFade; t += Time.deltaTime / TimeToFade)
        {
            Color newC = new Color(CC.r, CC.g, CC.b, Mathf.Lerp(CC.a, FadeTo, t));
            MR.material.color = newC;
            yield return null;
        }
    }

    IEnumerator StartPowerUp()
    {
        yield return new WaitForSeconds(0.1f); //Wait just enough to ball bounce once
        powerupEvent = powerUpState.on;        //Activate power up, raising an event
    }
    

	//Check the time and if timer is up, stop the powerup effect.
    //Issue: if we touch two powerups one after the other, before the first time end, the time DOES NOT extend.
    //However, that is strange, because, IT WOULD WORK. 
    //The challenge here is: what is wrong with it?
	private void FixedUpdate()
	{
		//If powerup is off , do nothing.
        if (powerupEvent == powerUpState.off) return;
        //If there was a time to change
        if (timeWait!=0.0f)
        {
            waitUntil = Time.time + timeWait; 
            timeWait = 0.0f;

        } else
        {
	        if (Time.time<waitUntil) return;
            waitUntil = 0.0f;
            powerupEvent = powerUpState.off;
	        StartCoroutine(InvulnerableIsOff()); //Give a window for the player resume playing.
        }
    }
    
	private IEnumerator InvulnerableIsOff(){
		yield return new WaitForSeconds(0.5f);
		Invulnerable=false;
	}

    /**************************Position PowerUp in the Structure*******************************************************************/
    /// <summary>
    /// given the quantity of helix of the screen, estimates how and where put the powerups.
    /// </summary>
    /// <param name="helixCount"></param>
    /// <returns></returns>
    public static List<int> definePowerUpLevels(int helixCount)
    {
        List<int> levelList = new List<int>();
        const int powerUplimit = 25; //max 1 powerUp every 25 helix
        //The powerup should be also separated with some level in between to avoid effect overlaping if possible.
        int powerUpAmount = helixCount / powerUplimit; //if it is zero, no powerup to the level.
                                                       //the calc are following: position powerup between levelx=1-5, add 25-x level and repeat the calculation.
        int nextLevel = 0;
        int currentLevel = 0;
        nextLevel = 5 + UnityEngine.Random.Range(1, 5);
        currentLevel += nextLevel;

        for (int i = 0; i < powerUpAmount; i++)
        {
            levelList.Add(nextLevel);
            nextLevel += powerUplimit ;
            nextLevel += UnityEngine.Random.Range(1, 5);
        }
        return levelList;
    }
}

//Improved: Powerup handles all the variables affecting the player's behavior
//20210404:Added Invulnerable variable to check conditions and maximizing player chances.
//I don't really like the idea of making the powerup duration variable, due this makes
//the game unpredictable. So I will turn it off
//Improved code, simplified in about 15%
//20210404: Very strange case happened without explanation: variable timeWait that for some reason
//resets itself to zero without reason.
//I commented ALL the places where the variable could be reset or changed, and for my surprise
//IT CONTINUED TO RESET ITSELF TO ZERO. I have no explanation.
//Workaound: initialize vara to TIME_EFFECT solved the issue.
