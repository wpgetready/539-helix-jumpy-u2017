﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for making pieces moving according several patterns.
/// </summary>
public class HelixPieceController : MonoBehaviour {

    private Rigidbody rigid = null;
    private MeshCollider meshCollider = null;
    private MeshRenderer meshRender = null;
    private Renderer render = null;
    
    private void CacheComponents()
    {
        //Cache components
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if (meshCollider == null)
            meshCollider = GetComponent<MeshCollider>();
        if (meshRender == null)
            meshRender = GetComponent<MeshRenderer>();
        if (render == null)
            render = GetComponent<Renderer>();
    }

    /// <summary>
    /// Disable this piece
    /// </summary>
    public void Disable()
    {
        CacheComponents();
        meshRender.enabled = false;
        meshCollider.enabled = false;
    }


    /// <summary>
    /// Set this helix piece as mortal piece. If the player touche a mortal piece
    /// (without a Powerup) he/she dies.
    /// </summary>
    public void SetMortalPiece()
    {
        CacheComponents();
        gameObject.tag = "Finish";
        meshRender.material = GameManager.Instance.deadPieceMaterial;
    }
    public void SetDeadPieceWithMov(bool oneDirection, bool lerpMov, int angleMov, float vel, float velLerp, float dstMeta)
    {
        CacheComponents();
        gameObject.tag = "Finish";
        meshRender.material = GameManager.Instance.deadPieceMaterial;

        transform.localScale += Vector3.one * 0.01f;

        StartCoroutine(MovPiece(oneDirection, lerpMov, angleMov, vel, velLerp, dstMeta));
    }
    /// <summary>
    /// The Tower will be a composition of moving and static objects, according the patterns defined on every level
    /// </summary>
    /// <param name="oneDirection"></param>
    /// <param name="lerpMov"></param>
    /// <param name="angleMov"></param>
    /// <param name="vel"></param>
    /// <param name="velLerp"></param>
    /// <param name="dstMeta"></param>
    /// <returns></returns>
    IEnumerator MovPiece(bool oneDirection, bool lerpMov, int angleMov, float vel, float velLerp, float dstMeta)
    {
        float initialAngle = transform.localEulerAngles.y;
        float totalRotacion = angleMov;
        float tLerp = 0;
        float rotTarget = 0;
        transform.localEulerAngles = new Vector3(transform.eulerAngles.x, initialAngle + totalRotacion, transform.eulerAngles.z);
        //There is some restrictions applied which are not clear enough if you see a level, nor the level configuration
        //To configure levels, go to Scripts->ScriptableObjects->Game1->Levels
        //1-If you select OneDirection, selecting lerpMov has no effect.

        while (true)
        {
            if (oneDirection)
            {
                if (lerpMov)
                {
                    totalRotacion = 0;
                    while (true)
                    {
                        tLerp = 0;
                        rotTarget = totalRotacion + angleMov;
                        while (totalRotacion < rotTarget - dstMeta)
                        {
                            tLerp += velLerp * Time.deltaTime;
                            totalRotacion = Mathf.Lerp(totalRotacion, rotTarget, tLerp);
                            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, initialAngle + totalRotacion, transform.localEulerAngles.z);
                            yield return null;
                        }
                        yield return null;
                    }
                }
                else
                {
                    while (true)
                    {
                        totalRotacion += vel * Time.deltaTime;
                        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, initialAngle + totalRotacion, transform.localEulerAngles.z);
                        yield return null;
                    }
                }
            }
            else
            {
                if (lerpMov)
                {
                    while (true)
                    {
                        tLerp = 0;
                        while (totalRotacion < angleMov - dstMeta)
                        {
                            tLerp += velLerp * Time.deltaTime;
                            totalRotacion = Mathf.Lerp(totalRotacion, angleMov, tLerp);
                            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, initialAngle + totalRotacion, transform.localEulerAngles.z);
                            yield return null;
                        }
                        tLerp = 0;
                        while (totalRotacion > -angleMov + dstMeta)
                        {
                            tLerp += velLerp * Time.deltaTime;
                            totalRotacion = Mathf.Lerp(totalRotacion, -angleMov, tLerp);
                            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, initialAngle + totalRotacion, transform.localEulerAngles.z);
                            yield return null;
                        }
                        yield return null;
                    }
                }
                else
                {
                    while (true)
                    {
                        while (totalRotacion < angleMov - 0.02f)
                        {
                            totalRotacion += vel * Time.deltaTime;
                            if (totalRotacion >= angleMov) totalRotacion = angleMov - 0.01f;
                            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, initialAngle + totalRotacion, transform.localEulerAngles.z);
                            yield return null;
                        }
                        while (totalRotacion >  -angleMov + 0.02f)
                        {
                            totalRotacion -= vel * Time.deltaTime;
                            if (totalRotacion < -angleMov) totalRotacion = -angleMov + 0.01f;
                            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, initialAngle + totalRotacion, transform.localEulerAngles.z);
                            yield return null;
                        }
                        yield return null;
                    }
                }
            }
            yield return null;
        }
        
    }

    /// <summary>
    /// Set this helix piece as normal piece
    /// </summary>
    public void SetNormalPiece()
    {
        CacheComponents();
        meshRender.material = GameManager.Instance.normalPieceMaterial;
    }


    public void Shatter()
    {
        if (meshRender.enabled)
        {
            StartCoroutine(Shattering());
        }    
    }
    private IEnumerator Shattering()
    {
        meshRender.material = GameManager.Instance.brokenPieceMaterial;
        meshCollider.enabled = false;
        Vector3 forcePoint = transform.parent.position;
        transform.parent = null;
        Vector3 point_1 = transform.position;
        Vector3 point_2 = meshRender.bounds.center + Vector3.up * (meshRender.bounds.size.y / 2f);
        Vector3 dir = (point_2 - point_1).normalized;
        rigid.isKinematic = false;
        rigid.AddForceAtPosition(dir * 10f, forcePoint, ForceMode.Impulse);
        rigid.AddTorque(Vector3.left * 100f);
        rigid.velocity = Vector3.down * 10f;
        yield return new WaitForSeconds(5f);
        rigid.isKinematic = true;
        Disable();
    }

}
