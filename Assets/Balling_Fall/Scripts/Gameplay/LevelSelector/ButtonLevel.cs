﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonLevel : MonoBehaviour {
    public playerRecord CompletedChallengesArray;
	// Use this for initialization
	void Start () {
        Button btn = gameObject.GetComponent<Button>();
        //Get data from the text
        String text = btn.GetComponentInChildren<Text>().text;
        btn.onClick.AddListener(()=> Clicked(int.Parse(text)));
	}

    private void Clicked(int levelNumber)
    {
        Debug.Log("Level selected: " + levelNumber);
        //CompletedChallengesArray.Completed[levelNumber] = true;
        SceneManager.LoadScene(levelNumber);

    }

}
