﻿using BallingFallGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using TMPro;

    //20210406: I'm having some difficulty with this implementation, mostly due the poor documentation... :(

public class LevelSelector : MonoBehaviour
{
    
    public Button ChallengeBtn;
    public Button InfiniteBtn;
    public Button GoBack;
    
    public GameplayConfig gameConfig; //current challenge tracking. Infinite tracking does use this data,at least the completed field, only for tracking reasons.
    public Sprite enabled;
    public Sprite disabled;
    public Text gameName;
    public GameObject mainPanel;
    public GameObject levelHolder;
    public GameObject levelIcon;
    public GameObject thisCanvas;
    public int numberOfLevels = 50; //WHAT IS THIS?
    public Vector2 iconSpacing;
    private Rect panelDimensions;
    private Rect iconDimensions;
    private int amountPerPage;
    private int currentLevelCount;
    private playerRecord chData = new playerRecord( GameMode.Challenge);
    private playerRecord infData = new playerRecord(GameMode.Infinite);

    private PageSwiper swiper = null;
    private GridLayoutGroup grid = null;
   // private playerRecord chData = new playerRecord();
    // Start is called before the first frame update
    void Start()
    {
        //20210406: since we persist data, we can load data before start. Notice however that
        //the case of empty is not fully solved.
        gameName.text = gameConfig.appConfig.DisplayName;
        reloadData();
        SetupPanels();
    }

    private void reloadData()
    {
        chData.LoadJson();
        infData.LoadJson();
        setupButtons(); //get info from tracking data to display in the buttons.
    }

    /// <summary>
    /// Get info from tracking data, and set the buttons with appropiate values.
    /// </summary>
    private void setupButtons()
    {
        Text challengeTxt = ChallengeBtn.GetComponentInChildren<Text>();
        challengeTxt.text = "CHALLENGE (LEVEL " + chData.getIndex() + ")";
        Text infiniteTxt = InfiniteBtn.GetComponentInChildren<Text>();
        infiniteTxt.text = "INFINITE (LEVEL " + infData.getIndex() + ")";
    }

    private void SetupPanels()
    {
        panelDimensions = levelHolder.GetComponent<RectTransform>().rect;
        iconDimensions = levelIcon.GetComponent<RectTransform>().rect;
        //Find how many icons fit in a Row
        int maxInARow = Mathf.FloorToInt((panelDimensions.width + iconSpacing.x) / (iconDimensions.width + iconSpacing.x));
        //Find how many icons fit in a Column
        int maxInACol = Mathf.FloorToInt((panelDimensions.height + iconSpacing.y) / (iconDimensions.height + iconSpacing.y));
        //That's will give how many icons fit in a page.
        amountPerPage = maxInARow * maxInACol;
        //And since numberOfLevels are provided, we can estimate how many pages to display.
        int totalPages = Mathf.CeilToInt((float)numberOfLevels / amountPerPage);
        LoadPanels(totalPages);
    }

    void LoadPanels(int numberOfPanels)
    {
        
        GameObject panelClone = Instantiate(levelHolder) as GameObject;
      //  if (swiper != null) Destroy(swiper); //this can happen if we reset the collection.
        swiper = levelHolder.AddComponent<PageSwiper>();
     
        swiper.totalPages = numberOfPanels;

        for (int i = 1; i <= numberOfPanels; i++)
        {
            GameObject panel = Instantiate(panelClone) as GameObject;
            panel.transform.SetParent(thisCanvas.transform, false);
            panel.transform.SetParent(levelHolder.transform);
            panel.name = "Page-" + i;
            panel.GetComponent<RectTransform>().localPosition = new Vector2(panelDimensions.width * (i - 1), 0);
            SetUpGrid(panel);
            int numberOfIcons = i == numberOfPanels ? numberOfLevels - currentLevelCount : amountPerPage;
            LoadIcons(numberOfIcons, panel);
        }
        Destroy(panelClone);
    }

    void SetUpGrid(GameObject panel)
    {
        //if (grid!= null) Destroy(grid);
        grid = panel.AddComponent<GridLayoutGroup>();
        grid.cellSize = new Vector2(iconDimensions.width, iconDimensions.height);
        grid.childAlignment = TextAnchor.MiddleCenter;
        grid.spacing = iconSpacing;
    }

    /// <summary>
    /// Ok this routine ended up a little more sophisticated than I anticipated
    /// </summary>
    /// <param name="numberOfIcons"></param>
    /// <param name="parentObject"></param>
    void LoadIcons(int numberOfIcons, GameObject parentObject)
    {
        for (int i = 0; i < numberOfIcons; i++)
        {
            currentLevelCount++;
            GameObject icon = Instantiate(levelIcon) as GameObject;
            icon.transform.SetParent(thisCanvas.transform, false);
            icon.transform.SetParent(parentObject.transform);
            icon.name = "Level " + currentLevelCount;
            icon.GetComponentInChildren<Button>().GetComponentInChildren<Text>().text = currentLevelCount.ToString();

            int idx = chData.trackingLevel.FindIndex(x => x.level == currentLevelCount);
            if (idx!=-1)
            {
                    icon.GetComponent<Image>().sprite = enabled;
                    icon.GetComponent<Button>().onClick.AddListener(() => challengeClicked(int.Parse(icon.GetComponent<Button>().GetComponentInChildren<Text>().text)));
            }
            else //it was never completed
            {
                    //we cannot interact with icon in disable mode.
                    icon.GetComponent<Button>().interactable = false;
                    icon.GetComponent<Button>().GetComponentInChildren<Text>().enabled = false; //don't show numbers
            }
            
        }
    }

    private void challengeClicked(int levelNumber)
    {
        Debug.Log("Level selected: " + levelNumber);
        gameConfig.currentGameMode = GameMode.Challenge;
        chData.currentLevel = levelNumber;
        chData.SaveJson();
        //GameManager.Instance.challengeData.currentLevel = levelNumber;
        SceneManager.LoadScene(1); //equivalente to SceneManager.LoadScene("GamePlay"). See File->BuildSettings
    }

    public void PlayButtonSound()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.button);
    }

    public void PlayChallenge()
    {
        //   GameModeConfiguration.gameMode = GameMode.Challenge; //Recall this will change game configuration before starting the game
        //Hide Main Panel, and show Challenge Panel
        //mainPanelChallenge.SetActive(true);
        GoBack.gameObject.SetActive(true);
        mainPanel.SetActive(false);
        levelHolder.SetActive(true);
        levelHolder.SetActiveRecursively(true);
    }

    //Back to the menu from Challenge Mode
    public void GoBackMenu()
    {
        //mainPanelChallenge.SetActive(false);
        GoBack.gameObject.SetActive(false);
        mainPanel.SetActive(true);
        levelHolder.SetActive(false);
        //levelHolder.SetActiveRecursively(false);
    }

    public void PlayInfinite()
    {

        gameConfig.currentGameMode = GameMode.Infinite;
        //gameConfig.infiniteTOR.saveMode =true;
        SceneManager.LoadScene(1);
    }

    public void ResetChallenge()
    {
        chData.reset();
        reloadData();
        resetLevels();
    }
    public void ResetInfinite()
    {
        infData.reset();
        reloadData();
    }

/// <summary>
/// Reset the levels to the initial collection.
/// </summary>
    private void resetLevels()
    {
        //Obtener todos los paneles
        Button[] buttons = levelHolder.GetComponentsInChildren<Button>(true);
        Debug.Log("Buttons detected:" + buttons.Length);
        buttons[0].interactable = true;
        buttons[0].GetComponentInChildren<Text>().enabled = true;

        for (int i = 1; i<buttons.Length-1;i++)
        {
            buttons[i].GetComponent<Image>().sprite = disabled;
            buttons[i].interactable = false;
            buttons[i].GetComponentInChildren<Text>().enabled = false;
        }
    }
    
}

/*
 * Esta restando un problema un poco idiota, pero no es tanto:
 * -Cuando se resetea Challenge, es preciso recargar la interfaz.
 * El problema es que la interfaz YA fue cargada, y no es posible recargarla porque implica duplicacion de objetos que YA existen.
 * Por lo tanto, como logro resetear los iconos?*/

