﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// This structure tracks progress data when playing modes (currently Challenge and infinite)
/// The only one difference between both is the saveMode, which only works for Infinite and is used to record data to add later to Challenge Mode.
/// </summary>
/// 
//levelCompleteData NO puede ser un array. Tiene que ser una lista, debido a que el tracking es infinito.

[System.Serializable]
public enum gameType { Challenge, Infinite, Practice }

//Data needs to be serializable in order to be converted to Json.
[System.Serializable]
public struct levelCompleteData
{
    /*
    public levelCompleteData(int level,int played,double bestTime,int bestScore,int deaths)
    {
        this.level = level;
        this.played = played;
        this.bestTime = bestTime;
        this.bestScore = bestScore;
        this.deaths = deaths;
    }
    */
    //20210407: Currently this data isn't saved and isn't used.
    //
    public int level; //level tracked
    public int played; //How many times this game was played
    public int bestTime; //best time in miliseconds
    public string bestTimeDate; //when the bestTime was recorded.
    //public int bestStars; //best quantity of stars
	public int bestScore; //best single score for this level.
	public int deaths; //how many times the player has dead in this level.
}

//Note: The object is a ScriptableObject, so we have the chance of changing data on the fly from Unity Editor.
//20210407: This class isn't Scriptable Object anymore, since this class is persisted using JsonUtility.
//JsonUtility can save data serialized, however IT CAN'T RECOVER IF THE CLASS IS A SCRIPTABLE OBJECT.
//Therefor, the SO was dropped.
[CreateAssetMenu(fileName = "TOR", menuName = "Game/RegisterPlayerRecord", order = 8)]
[System.Serializable]
public class playerRecord {
    private string filePath;

    public playerRecord(GameMode gm)
    {
        switch (gm)
        {
            case GameMode.Challenge:
                filePath =  "/challengeTOR.json";
                break;
            case GameMode.Infinite:
                filePath = "/infiniteTOR.json";
                break;
            default:
                break;
        }
    }

    public int totalTime; //total time for all levels played so far, only the passed levels.
    public int totalTimePlayed;//total time for all levels played including deaths and everything.
    public int currentLevel; //level currently played for the Player. DO NOT confuse with the level tracked in levelCompleteData
    public int currentScore; //Current player score
    public int bestScore;    //Best scores so far
    public int deathCounter; //Player death count
    public bool saveMode = false; //Internal for saving Only Infinite Mode
    public List<levelCompleteData> trackingLevel;

    /// <summary>
    /// Saves class data to file. 
    /// </summary>
    /// <param name="challenge">true if it is challenge, false if it is infinite</param>
    public void SaveJson()
	{
        string jsonData = JsonUtility.ToJson(this);
        File.WriteAllText(Application.persistentDataPath + filePath, jsonData);//If the file exist, it is overwritten
    }
    
        /// <summary>
        /// Load a playerRecord from file and update the class.
        /// Return true if success, false if file not found.
        /// </summary>
        /// <param name="challenge"></param>
        /// <returns></returns>
    public bool LoadJson ()
    {
        if (!File.Exists( Application.persistentDataPath + filePath))
        {
            this.trackingLevel = new List<levelCompleteData>();  //Initialize list.
            levelCompleteData lcd = new levelCompleteData();
            lcd.level = 1;
            lcd.bestTime = 999999; //this time needs to be higher because we'll keep the lowest
            lcd.bestTimeDate = System.DateTime.Now.ToString();
            lcd.bestScore = 0;
            lcd.deaths = 0;
            lcd.played = 0;

            this.trackingLevel.Add(lcd);
            this.currentLevel = 1;
            this.currentScore = 0;
            this.bestScore = 0;
            this.deathCounter = 0;
            this.saveMode = false;
            this.totalTime = 0;
            this.totalTimePlayed = 0;
            SaveJson();
            return false; //Trying to load a inexistent file, return false
        }
            
        string j =File.ReadAllText(Application.persistentDataPath +  filePath);
        playerRecord p= JsonUtility.FromJson<playerRecord>(j);
        //Reassign data to current class.
        currentLevel = p.currentLevel;
        currentScore = p.currentScore;
        bestScore = p.bestScore;
        deathCounter = p.deathCounter;
        saveMode = p.saveMode;
        trackingLevel = p.trackingLevel;
        totalTime = p.totalTime;
        totalTimePlayed = p.totalTimePlayed;
        return true;
    }

    /// <summary>
    /// Reset Tracking: if posible,it erase the file and start over
    /// </summary>
    public void reset()
    {
        if (File.Exists(Application.persistentDataPath + filePath))
        {
            File.Delete(Application.persistentDataPath + filePath);
            LoadJson();
        }
    }

    public int getIndex()
    {
        if (this.trackingLevel.Count == 0) return 1;
        levelCompleteData lcd = this.trackingLevel[this.trackingLevel.Count - 1];
        return lcd.level;
    }
 
}