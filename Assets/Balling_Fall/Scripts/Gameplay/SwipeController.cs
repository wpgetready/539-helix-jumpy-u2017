﻿using System;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    [SerializeField] private float rotatingSpeed = 50f;

    private Vector2 fingerDownPosition;
    private Vector2 fingerUpPosition;

    [SerializeField]
    private bool detectSwipeOnlyAfterRelease = false;

    [SerializeField]
    private float minDistanceForSwipe = 20f;

    public static event Action<SwipeData> OnSwipe = delegate { };

    private void Start()
    {
        if (!Input.touchSupported) Debug.Log("Touch unsupported!");
        if (!Input.mousePresent) Debug.Log("Mouse missing!");
        
    }

    private void OnEnable()
    {
        SwipeController.OnSwipe += SwipeDetector_OnSwipe;
    }

    private void OnDisable()
    {
        SwipeController.OnSwipe -= SwipeDetector_OnSwipe;
    }


    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        Debug.Log("Swipe in Direction: " + data.Direction);

        switch (data.Direction)
        {
            case SwipeDirection.Up:
                break;
            case SwipeDirection.Down:
                break;
            case SwipeDirection.Left:
                Rotate(0.1f);
                break;
            case SwipeDirection.Right:
                Rotate(1.0f);
                break;
            default:
                break;
        }
    }

    private void Update()
    {
    
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUpPosition = touch.position;
                fingerDownPosition = touch.position;
            }

            if (!detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved)
            {
                fingerDownPosition = touch.position;
                DetectSwipe();
            }

            if (touch.phase == TouchPhase.Ended)
            {
                fingerDownPosition = touch.position;
                DetectSwipe();
            }
        }
        //FZSM My first version of mouse equivalent
        //There is one condition missing that is when the mouse moves but we
        //do not release the mouse button.
        //How do I solve this?
        
        if (Input.GetMouseButtonDown(0))
        {
            fingerUpPosition = Input.mousePosition;
            fingerDownPosition = Input.mousePosition;
        }

        if (!detectSwipeOnlyAfterRelease && Input.GetMouseButton(0))
        {
            fingerDownPosition = Input.mousePosition;
            DetectSwipe();
        }

        if (Input.GetMouseButtonUp(0))
        {
            fingerDownPosition = Input.mousePosition;
            DetectSwipe();
        }
    }

    private void DetectSwipe()
    {
        if (SwipeDistanceCheckMet())
        {
            if (IsVerticalSwipe())
            {
                var direction = fingerDownPosition.y - fingerUpPosition.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
                SendSwipe(direction);
            }
            else
            {
                var direction = fingerDownPosition.x - fingerUpPosition.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                SendSwipe(direction);
            }
            fingerUpPosition = fingerDownPosition;
        }
    }

    private bool IsVerticalSwipe()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }

    private bool SwipeDistanceCheckMet()
    {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }

    private float VerticalMovementDistance()
    {
        return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
    }

    private float HorizontalMovementDistance()
    {
        return Mathf.Abs(fingerDownPosition.x - fingerUpPosition.x);
    }

    private void SendSwipe(SwipeDirection direction)
    {
        SwipeData swipeData = new SwipeData()
        {
            Direction = direction,
            StartPosition = fingerDownPosition,
            EndPosition = fingerUpPosition
        };
        OnSwipe(swipeData);
    }


    private void Rotate(float x)
    {
        //1.5f if we use keyboard, needs to find what is the best for swipe
        float multiplier = 4.5f; //Used to make rotatin faster
        if (x <= 0.5f) //Touch left
        {
            transform.eulerAngles += Vector3.up * rotatingSpeed * Time.deltaTime * multiplier;
        }
        else //Touch right
        {
            transform.eulerAngles += Vector3.down * rotatingSpeed * Time.deltaTime * multiplier;
        }
    }
}

public struct SwipeData
{
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public SwipeDirection Direction;
}

public enum SwipeDirection
{
    Up,
    Down,
    Left,
    Right
}