﻿using BallingFallGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelixDetector : MonoBehaviour {

    public static int PassedCount { private set; get; }

    public delegate void DetectorEvents(int passedCount);
    public static event DetectorEvents PastPieces;
    
    private void OnEnable()   {        PowerUp.PowerUpEvent += PowerUp_PowerUpEvent;    }
    private void OnDisable()  {        PowerUp.PowerUpEvent -= PowerUp_PowerUpEvent;    }
    private void PowerUp_PowerUpEvent(powerUpState obj)    {

        switch (obj)
        {
        	case powerUpState.on:
	        	//20210404: This case does not happen again, since it was fixed on PowerUp class..
	            // if (freeFall) PassedCount = 0; //This is the case of when touching two powerups one after another.
	            //freeFall = true;
                break;
            case powerUpState.off:
          //      Debug.Log("Level Passed:" + PassedCount.ToString());
                PassedCount = 0;
                break;
            default:
                break;
        }
    }

    private int currentHelix = -1;

    /// <summary>
    /// 20210407: This collider is attached to the player.
    /// Also, this collider hast two objects, a box collider (with isTrigger active, which is VERY IMPORTANT to be active)
    /// and a Rigidbody attached, according with 
    /// https://docs.unity3d.com/ScriptReference/Collider.OnTriggerEnter.html
    /// Read very carefully!!!
    /// Also the helix prefab has a tag named Helix, which allows easily discard any other object with different tag name
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Helix")) return;
        //Now that is working, the issue is we can detect the same object mutiple times .
        //So it is very important avoid multiple collisions with the same object (if everything is correctly set, that will happen!)
        if (other.GetInstanceID() == currentHelix)
        {
            Debug.Log("RE Detected:" + other.tag);
            return;
        }

        //We are asumming that Player is colliding Helix Tag. Is not that the case, improve this function.
        //Remember current instance to avoid detection repetition.
        currentHelix = other.GetInstanceID();
        //Don't go beyond maximum scale of sounds provided.
        int currentP = Mathf.RoundToInt(Mathf.Min(PassedCount, SoundManager.Instance.SMC.passedPieces.Length - 1));

	    if (PowerUp.currentPU==powerUpState.on)
        {
	        //Debug.Log("Nivel en cuenta:" + parent.transform.position.y);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.passedPieces[currentP]);
	        PassedCount++;
            return;
        }

        if (SoundManager.Instance.SMC.passedPieces.Length != 0)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.passedPieces[currentP]);
        }
        else
        {
            Debug.LogError("There are no sound for pieces in Passed Pieces");
        }
        PassedCount++;
    }

    public static void ResetPassedCount()
    {
        ResetPassedCount(false);
    }

    public static void ResetPassedCount(bool freeFall)
    {
        if (PastPieces != null && PassedCount > 0)
        {
            PastPieces.Invoke(PassedCount);
        }
       if (!freeFall) PassedCount = 0;

    }
}

/*
 * The idea is VERY simple: just attach to the player a volume, big enough to collide with the helixes.
 * In fact, the collider is BIG (maybe unnecesary big)
 * There is only one posible exception: if the helix is empty, ther won't be collisions.
 * 
 */