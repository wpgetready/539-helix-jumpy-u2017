﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Indicator of a GameTrack is Open (in use) or Closed (it was saved and it is not recorded
//anymore)

[System.Serializable]
public enum gameStatus { Open,Close }

//Result of playing a level
//death=the player died, pass=the player pass the level
//unknown=initial play status. If a game has this value is because the play
//is running or the app closed unexpectedly when playing.

[System.Serializable]
public enum playResult { death, pass, unknown }

[System.Serializable]
public class GameTracking  {
    System.DateTime startTime; //time this game started 
    int DeathCounterOverall; //Sum of ALL game tracks combined
    int BestScoreOverall; //Best scores of all game tracks combined
    int elapsedOverall; //all elapsed combined
    int elapsedEffectiveOverall; //all effective combined
    public List<GameTrack> GameList;
}

[System.Serializable]
public class GameTrack
{
    gameType GameType; //Game Type
    int CurrentLevel; //current level in this GameTrack
                       //If the game is Closed, and GameTrack is Challenge,
                       //this will point to the last level played
                       //In Practice Mode is't exactly the same, since the player can 
                       //play ANY game.
    int BestScore; //Best Score in this game track so far
    int DeathCounter; //# of death on this GameTrack
    gameStatus Status; //Check if a game is Open/Closed.
    //In any moment only 3 games can be opened: a Practice, A Challenge and an Infinite.
    System.DateTime startTime; //When this game track started
    System.DateTime ?endTime = null; //nullable because Status is Open
    int elapsed; //Time in seconds from endTime-starTime
    int elapsedEffective; //Time in seconds of game effectively elapsed
    //This holds how much time in the GameTrack the player has been playing
    //This a sum of all the games played in this GameTrack
    public List<LevelTrack> levels; //List of levels played
}

[System.Serializable]
public class LevelTrack
{
    int level; //Level played
    int death; //# of death for this level
    double BestTime; //Best time in seconds for this level (Practice Mode)
    double BestScore; //Best Score for this
    int Played; //how many times this games has been played
    List<Play> levelPlayed; //list of games layed
}
[System.Serializable]
public class Play
{
    System.DateTime startTime;
    System.DateTime endTime;
    double elapsed; //time in seconds to miliseconds for example 15.456
    int score; //points gotten in this play
    playResult status;
}


/*
 * The idea is tracking data from all the games played with this app.
 * This will be used to help the player make its own speedrun and track their progress.
 * 
 * So the basic concepts are like this:
 
    The app is tracking all the games played for the player.
    Everytime a game is started from the main menu, a GameTrack is created.
    If there was any Open GameTrack, it is closed and a new is created.

    There will be 3 kind of possible games: Practice, Challenge and Infinite
    -Practice is where the player can practice ANY game from Challenge.
    He/She can play ANY level he/she want in any arbitrary order.
    All these games record data to be used in the future.

    -Challenge: Challenge is where the speedrun is made. In Challenge you can start from
    level 1 and go on, until the level 100 (where it will be the last level).
    The difference with Practice is you shouldn't go back or try other levels with 
    Challenge Mode (you could but I wouldn't do it, since you have Practice Game for that).
    The target is complete the challenge in the best time and most point possible,
    even without losing lifes (something I firmly believe this is impossible, but
    who knows what kind of human are out there).

    Challenge, as Practice, share the SAME levels and they run the same way, over and over. 

    -Infinite: Infinite is a infinite practice level, where every level is created 
    randomly and procedurally. Theoretically there is no limit level, however probably
    the limit would be about 1000.
    Since always change, there is no sense to make a speedrun in this level, because there is no comparison to be made. 

    In short, GameTracking tries to register all the times and records for speedrunning
    with this game.

    However, it records somewhat differente things for Practice,Challenge and Infinite games, since they are strictly and slightly different.


    20210413: This is has been posponed to posterior versions.
 * */
