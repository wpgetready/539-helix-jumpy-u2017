﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using BallingFallGame;
using UnityEngine.UI;
using System.Globalization;
using System;

/// <summary>
/// The ORDER IS IMPORTANT AND IT HAS TO MATCH THE PANELS IN THE ARRAY.
/// None will select NO screen, hiding everyone.
/// 
/// </summary>
public enum panelList { startGame, lostLevel,passLevel, pauseGame, gamePlay, revivePlayer, challengeCompleted,None };

public class UIManager : MonoBehaviour {

    public static UIManager Instance { private set; get; }


    [Header("Game Play configuration")]
    [SerializeField]GameplayConfig GPC;

    [SerializeField]
    [Header("Panels to manage. CAREFUL: panelList enum has match this collection")]

    //Used to control the panels without much codd
    [Tooltip("panel to be shown")]
    public panelList panel;

    //Panels needs to be put in the SAME order as in the enum panelList
    [Tooltip("List of panels this class will manage")]
    public GameObject[] Panels;

    [Header("Gameplay UI")]
    [SerializeField] private Text levelTxt;
    [SerializeField] private GameObject pauseBtn;
    [SerializeField] private GameObject unPauseBtn;
    [SerializeField] private GameObject timeBarUI; //20191005: needed to perform animation
    [SerializeField] private Image timeBar;
    [SerializeField] private Color colorNormal, colorAttention, colorDanger;
    [SerializeField] private Text textLevel;
    [SerializeField] private Image progressMask;

	[Header("Revive UI")]

    [SerializeField] private Image reviveCoverImg;

	[Header("Game Over UI")]
    [SerializeField] private GameObject starCoverUI;
    [SerializeField] private GameObject starUI;
    [SerializeField] private GameObject star_1;
    [SerializeField] private GameObject star_2;
    [SerializeField] private GameObject star_3;
    [SerializeField] private GameObject playBtns;
    [SerializeField] private GameObject playBtn;
    [SerializeField] private GameObject nextBtn;
    [SerializeField] private GameObject restartBtn;
    [SerializeField] private GameObject shareBtn;
    [SerializeField] private GameObject soundOnBtn;
    [SerializeField] private GameObject soundOffBtn;
    [SerializeField] private GameObject musicOnBtn;
    [SerializeField] private GameObject musicOffBtn;

    [SerializeField] private Image fadingPanel;

	[Header("References")]
    [SerializeField] private Animator settingAnim;
    [SerializeField] private Animator servicesAnim;
    [SerializeField] private Text gameNameText;

	[SerializeField] private AppConfig appConfig;
    
    [Header("Score Manager")]
    [SerializeField] private Text scoreText = null;
    [SerializeField] private Text addedScoreText = null;
    [SerializeField] private Text highScoreText = null;

    [Header("Panel Pass Level")]
    [SerializeField] private Text scoreText_endLevel = null;
    [SerializeField] private Text LvlTxt;
    [SerializeField] private Text TimeTxt;
    [SerializeField] private Text DiedTxt;
    [SerializeField] private Text TotalTimeTxt;

    [Header("Panel Lost Level")]
    [SerializeField] private Text ll_scoreText_endLevel = null;
    [SerializeField] private Text ll_LvlTxt;
    //[SerializeField] private Text TimeTxt;
    [SerializeField] private Text ll_DiedTxt;
    [SerializeField] private Text ll_TotalTimeTxt;

    [Header("For Controlling Input type")]
    [SerializeField] private GameObject Rotator;
    [SerializeField] private Button SwipeBtn;
    [SerializeField] private Button TwoHandsBtn;


    private float timeCount = 0;
    private const string triggerAnimTextLevel = "ShowInitialLevel";

    private float maxWidthParent = 0;
    private bool distanceCalculate = false;
    private Vector2 posRespawn;
    private float totalDistance = 0;
    Animator barAnimation; //20191005:Introduced to add some attention when the times goes off.

    private Animator scoreAnim = null;
    private const string SHOW_TEXT = "showText";
    public static int score = 0;
    int _oldValue;


    #region "Score functions"
    /// <summary>
    /// return the current highscore
    /// </summary>
    /// <returns></returns>
    private int getHighScore()
    {
        if (GameManager.Instance == null) return 0;
        return GameManager.Instance.trackData.bestScore;
    }

    /// <summary>
    /// set the highscore. 
    /// </summary>
    /// <param name="Score"></param>

    private void setHighScore(int Score)
    {
        GameManager.Instance.trackData.bestScore = Score;
    }

    /// <summary>
    /// resets the score. Used when testing level or restarting level.
    /// </summary>
    public static void resetScore()
    {
        score = 0;
    }
       
    /// <summary>
    /// Add value tu current score
    /// </summary>
    /// <param name="value"></param>
    public void addToScore(int value)
    {
        score += value;
        addToScore();
    }

    /// <summary>
    /// Update all the text objects using score.
    /// </summary>
    public void addToScore()
    {
        scoreText.text = score.ToString();
        scoreText_endLevel.text = "Score: " + score.ToString();
        ll_scoreText_endLevel.text = "Final Score: " + score.ToString();
    }

    /// <summary>
    /// Sets the highscore.
    /// </summary>
    /// <param name="score"></param>
    public void updateHighScore(int score)
    {
        if (score >= getHighScore())
        {
            setHighScore(score);
            highScoreText.text = "HIGH: " + string.Format("{0:#,#}", getHighScore());
        }
    }

    #endregion

    /// <summary>
    /// This class listen changes made in GameManager.
    /// </summary>
    private void OnEnable()
    {
        GameManager.GameStateChanged += GameManager_GameStateChanged;
        selectControlGameType(GPC.btnSwipe);
    }

    //Maintain connection with GameManager.
    private void OnDisable()
    {
        GameManager.GameStateChanged -= GameManager_GameStateChanged;
    }

    /// <summary>
    /// Listen the GameManager class and its events.
    /// </summary>
    /// <param name="obj"></param>
    private void GameManager_GameStateChanged(GameState obj)
    {
        switch (obj)
        {
            case GameState.Prepare:
                break;
            case GameState.Playing:
                if (!GameManager.Instance.IsRevived)
                {
                    //Reset time for player who see video.
                    timeCount = GameManager.Instance.TimeToPassLevel * 10.0f;
                    panel = panelList.gamePlay; //Change the panel to game Mode.
                    StartCoroutine(CountingDownTimeBar());
                }
                SetGameNameText(appConfig.DisplayName);
                break;
            case GameState.Pause:
                panel = panelList.pauseGame;
                break;
            case GameState.Revive:
                StartCoroutine(ShowReviveUI(0.5f));
                break;
            case GameState.PassLevel:
                StartCoroutine(ShowPassLevelUI(2.5f));         
				break;
            case GameState.GameOver:
                StartCoroutine(ShowGameOverUI(0.5f));
                break;
            case GameState.PassChallenge:
                panel = panelList.challengeCompleted;
                break;
            default:
                break;
        }
    }
    
    /// <summary>
    /// Updates screen with statistic info when the player pass the level
    /// </summary>
	private void passLevelUpdatePanel(){
        //Update time elapsed for this level
        double t = GameManager.elapsedTime;

        Debug.Log("Elapsed Time:" + t.ToString() + " s");
		TimeTxt.text = (t / 1000).ToString("F3",CultureInfo.InvariantCulture)+ "s";
        //Update level
        LvlTxt.text = "Lvl: " + GameManager.CurrentLevel;
        //Update how many dies we got currently
        DiedTxt.text = "Died: " + GameManager.Instance.trackData.deathCounter.ToString();
        //Update total time so far
        TotalTimeTxt.text = FormatTime(GameManager.Instance.trackData.totalTime);
    }

    /// <summary>
    /// Updates screen with statistic info when the player lost a life.
    /// </summary>
    private void lostLevelUpdatePanel()
    {
        //Update time elapsed for this level
        double t = GameManager.elapsedTime;

//        Debug.Log("Elapsed Time:" + t.ToString() + " s");
//        TimeTxt.text = (t / 1000).ToString("F3", CultureInfo.InvariantCulture) + "s";

        //Update level
        ll_LvlTxt.text = "Lvl: " + GameManager.CurrentLevel;
        //Update how many dies we got currently
        ll_DiedTxt.text = "Died: " + GameManager.Instance.trackData.deathCounter.ToString();
        //Update total time so far
        ll_TotalTimeTxt.text = FormatTime(GameManager.Instance.trackData.totalTime);
    }

    /// <summary>
    /// Converts miliseconds in seconds in the format ss.mmm
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    string FormatTime(float time)
    {
        int intTime = (int)(time/1000);
        int minutes = intTime / 60;
        int seconds = intTime % 60;
        float fraction = time - intTime * 1000;
        string timeText = String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
        return timeText;
    }
       
    void Awake()
    {
        if (Instance != null) DestroyImmediate(Instance.gameObject);
        Instance = this;
        //Initialize score objects
        scoreAnim = addedScoreText.GetComponent<Animator>();
    }

    void OnDestroy()
    {
        HelixDetector.PastPieces -= PieceExceeded;
        if (Instance == this) Instance = null;
    }

    void Start () {
        //Sets the panel when starting the game.

        _oldValue = (int)panel;
        enablePanel(_oldValue);

        HelixDetector.PastPieces += PieceExceeded;
        addToScore(); //Refresh score values, don't add anything

        if (!GameManager.IsRestart) //This is the first load
        {
            panel = panelList.startGame;
            timeBar.color = colorNormal;
            SetGameNameText(appConfig.DisplayName); //Get app name from configuration.
        }
        else
        {
            displayCurrentLevel(GameManager.CurrentLevel.ToString());
        }
        maxWidthParent = progressMask.transform.parent.GetComponent<RectTransform>().sizeDelta.x;
       // Debug.Log(maxWidthParent);
    }

    /// <summary>
    /// Enable the panel #number , disabling the rest.
    /// Tip: if we set to a no existent number, it will make all the panels invisible.
    /// </summary>
    /// <param name="number"></param>
    private void enablePanel(int number)
    {
        if (Panels.Length == 0)
        {
            Debug.LogError("Error: there is no panels");
            return;
        }

        if (number > Panels.Length)
        {
            Debug.LogError("Error: this number is above the number of panels available");
            return;
        }
        if (number < 0)
        {
            Debug.LogError("Error: number below zero not allowed");
            return;
        }
        //Iterate on all panels and enable only one of them, set false the rest.
        for (int i = 0; i < Panels.Length; i++)
        {
            Panels[i].SetActive(i == (int)panel);
        }
    }

    /// <summary>
    /// Display a message and start a animation (zoom and fading)
    /// </summary>
    /// <param name="msg"></param>
    public void displayCurrentLevel(string msg)
    {
        textLevel.text = msg;

        Animator anim = textLevel.GetComponent<Animator>();

        if (anim != null)
        {
            anim.SetTrigger(triggerAnimTextLevel);
        }
    }

    /// <summary>
    /// Set the name of the game
    // 20191012: This is a tackle to define the game for 3 different versions.
    // However it won't work on the start event because it is set ONE time
    // Following times the value takes the default in the Unity UI definition, so we need set every time the game starts
    /// </summary>
    private void SetGameNameText(string text)
    {
        gameNameText.text = text;
    }

    // Update is called once per frame
    void Update () {
        //check current panel and change it if necessary
        if (_oldValue != (int)panel)
        {
            _oldValue = (int)panel;
            enablePanel(_oldValue);

        }
        UpdateMusicButtons();
        UpdateMuteButtons();
        UpdateLevelProgress();
    }

    #region Buttons
    ///These buttons are directly connected to the UI   

    public void PlayButtonSound()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.button);
    }

    public void PauseBtn()
    {
        pauseBtn.SetActive(false);
        unPauseBtn.SetActive(true);
        Time.timeScale = 0;
    }
    public void UnPauseBtn()
    {
        pauseBtn.SetActive(true);
        unPauseBtn.SetActive(false);
        Time.timeScale = 1;
    }
    public void PlayBtn()
    {
        GameManager.Instance.PlayingGame();
        displayCurrentLevel(GameManager.CurrentLevel.ToString());
    }

    /// <summary>
    /// Connected directly to de UI
    /// </summary>
    public void RestartBtn()
    {
        GameManager.Instance.restartGame();
    }

    /// <summary>
    /// Connected directly to the button in the UI
    /// </summary>
    public void NextBtn()
    {
        GameManager.Instance.nextGame();
    }

    public void backMainMenu()
    {
        GameManager.Instance.backMainMenu();
    }

    public void NativeShareBtn()
    {
        ShareManager.Instance.NativeShare();
    }
    public void RateAppBtn()
    {
#if UNITY_IOS
        UnityEngine.iOS.Device.RequestStoreReview();
#elif UNITY_ANDROID
        Application.OpenURL(ShareManager.Instance.AppUrl);
#endif
    }
    public void SettingBtn()
    {
        servicesAnim.Play("ServicesBtns_Hide");
        settingAnim.Play("SettingBtns_Show");
    }
    public void ToggleSound()
    {
        SoundManager.Instance.ToggleMute();
    }

    public void ToggleMusic()
    {
        // SoundManager.Instance.ToggleMusic();
        PlayList.Instance.ToggleMusic();
    }
    public void BackBtn()
    {
        settingAnim.Play("SettingBtns_Hide");
        servicesAnim.Play("ServicesBtns_Show");
    }

    /* 20191008: Problem: if we dont have network, how do we suppose to work here?*/
    public void ReviveBtn()
    {
        panel = panelList.None; //Hides all panels.
        AdManager.Instance.ShowRewardedVideoAd();
    }

    public void SkipBtn()
    {

        panel = panelList.None;
        GameManager.Instance.GameOver();
    }

    #endregion

    /////////////////////////////Private functions
    private void UpdateMuteButtons()
    {
        if (SoundManager.Instance.GPC.musicIsMuted)
        {
            soundOnBtn.gameObject.SetActive(false);
            soundOffBtn.gameObject.SetActive(true);
        }
        else
        {
            soundOnBtn.gameObject.SetActive(true);
            soundOffBtn.gameObject.SetActive(false);
        }
    }

    private void UpdateMusicButtons()
    {
        if (PlayList.Instance.IsMusicOff())
        {
            musicOffBtn.gameObject.SetActive(true);
            musicOnBtn.gameObject.SetActive(false);
        }
        else
        {
            musicOffBtn.gameObject.SetActive(false);
            musicOnBtn.gameObject.SetActive(true);
        }
    }

    private void UpdateLevelProgress()
    {
        if(!distanceCalculate)
        {
            GameObject respawn = GameObject.FindWithTag("Respawn");
            if(respawn != null)
            {
                posRespawn = respawn.transform.position;
                totalDistance = Vector2.Distance(PlayerController.Instance.transform.position, posRespawn);

                distanceCalculate = true;

                float currentDistance = Vector2.Distance(PlayerController.Instance.transform.position, posRespawn);
                float width = maxWidthParent - (currentDistance * maxWidthParent / totalDistance);
            }
        }
        else if(distanceCalculate && PlayerController.Instance.PlayerState != PlayerState.PassLevel)
        {
            float currentDistance = Vector2.Distance(PlayerController.Instance.transform.position, posRespawn);
            float width = maxWidthParent - (currentDistance * maxWidthParent / totalDistance); // 400 = max width

            progressMask.rectTransform.sizeDelta = new Vector2(width, progressMask.rectTransform.sizeDelta.y);
        }
        else if(PlayerController.Instance.PlayerState == PlayerState.PassLevel)
        {
            progressMask.rectTransform.sizeDelta = new Vector2(maxWidthParent, progressMask.rectTransform.sizeDelta.y);
        }
    }

    private IEnumerator ShowGameOverUI(float delay)
    {
        lostLevelUpdatePanel();
        yield return new WaitForSeconds(delay);
        panel = panelList.lostLevel;
    }

    private IEnumerator ShowPassLevelUI(float delay)
    {
        passLevelUpdatePanel();//Display statististical data.

        yield return new WaitForSeconds(delay);

        panel = panelList.passLevel;

        //Disable Stars to calculate how many stars we have for screen.
        star_1.SetActive(false);
        star_2.SetActive(false);
        star_3.SetActive(false);

        float timeUse = GameManager.Instance.TimeToPassLevel - timeCount;
        float percent = (timeUse / GameManager.Instance.TimeToPassLevel) * 100f;

        float delayTime = 0.5f;
        if (percent >= GPC.threeStarPercentTime) //Show three stars
        {
            yield return new WaitForSeconds(delayTime);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.starCount);
            star_1.SetActive(true);
            yield return new WaitForSeconds(delayTime);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.starCount);
            star_2.SetActive(true);
            yield return new WaitForSeconds(delayTime);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.starCount);
            star_3.SetActive(true);
        }
        else if (percent >= GPC.twoStarPercentTime && percent < GPC.threeStarPercentTime ) //Show two stars
        {
            yield return new WaitForSeconds(delayTime);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.starCount);
            star_1.SetActive(true);
            yield return new WaitForSeconds(delayTime);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.starCount);
            star_2.SetActive(true);
            star_3.SetActive(false);
        }
        else //Show one star 
        {
            yield return new WaitForSeconds(delayTime);
            SoundManager.Instance.PlaySound(SoundManager.Instance.SMC.starCount);
            star_1.SetActive(true);
            star_2.SetActive(false);
            star_3.SetActive(false);
        }
    }

    private IEnumerator ShowReviveUI(float delay)
    {
        yield return new WaitForSeconds(delay);
        panel = panelList.revivePlayer;
        StartCoroutine(ReviveCountDown());
    }

    private IEnumerator ReviveCountDown()
    {
        float t = 0;
        while (t < GPC.reviveWaitTime )
        {
            if (panel!=panelList.revivePlayer)
                yield break;
            t += Time.deltaTime;
            float factor = t / GPC.reviveWaitTime;
            reviveCoverImg.fillAmount = Mathf.Lerp(1, 0, factor);
            yield return null;
        }
        panel = panelList.None;
        GameManager.Instance.GameOver();
    }
    private IEnumerator CountingDownTimeBar()
    {
        if (barAnimation == null)
        {
            barAnimation = timeBarUI.GetComponent<Animator>();
        }

        //Wait for finished fading
        while (!GameManager.Instance.IsFinishedFading)
        {
            yield return null;
        }

        timeCount = 0;
        while (timeCount < GameManager.Instance.TimeToPassLevel)
        {
            timeCount += Time.deltaTime;
            float factor = timeCount / GameManager.Instance.TimeToPassLevel;
            timeBar.fillAmount = Mathf.Lerp(1, 0, factor);
            //20191005:Change color according fill Amount
            
            if (timeBar.fillAmount>=0.6f)
            {
                timeBar.color = colorNormal;
                barAnimation.SetBool("danger", false);
            } else if (timeBar.fillAmount < 0.6f && timeBar.fillAmount>=0.3f)
            {
                timeBar.color = colorAttention;
                barAnimation.SetBool("danger", false);
            } else
            {
                timeBar.color = colorDanger;
                barAnimation.SetBool("danger", true);
            }
            
            yield return null;
            if (PlayerController.Instance.PlayerState == PlayerState.PassLevel)
                yield break;
            while (PlayerController.Instance.PlayerState != PlayerState.Living)
            {
                yield return null;
            }
        }
		//PlayList.Instance.StopMusic ();
        //Dying when time is up
        PlayerController.Instance.PlayerDies();
        GameManager.Instance.GameOver();
    }
         
    /// <summary>
    /// Fading the panel out with given fadingTime
    /// </summary>
    /// <param name="fadingTime"></param>
    public void FadeOutPanel(float fadingTime)
    {
        StartCoroutine(FadingOutPanel(fadingTime));
    }
    private IEnumerator FadingOutPanel(float fadingTime)
    {
        fadingPanel.gameObject.SetActive(true);
        float t = 0;
        Color startColor = fadingPanel.color;
        Color endColor = new Color(startColor.r, startColor.g, startColor.b, 0);
        while (t < fadingTime)
        {
            t += Time.deltaTime;
            float factor = t / fadingTime;
            fadingPanel.color = Color.Lerp(startColor, endColor, factor);
            yield return null;
        }
        fadingPanel.gameObject.SetActive(false);
    }

    /// <summary>
    /// Show level text with given level number
    /// </summary>
    /// <param name="level"></param>
    public void SetLevelTxt(int level)
    {
        if (GPC.testingLevel != 0)
        {
            levelTxt.text = "TEST : " + level.ToString();
            return;
        }
        levelTxt.text = "LEVEL: " + level.ToString();
    }

    private void PieceExceeded(int quantity)
    {
        int addedScore = 0;

        switch (quantity)
        {
            case 1:
                addedScore += 10;
                break;
            case 2:
                addedScore += 30;
                break;
            case 3:
                addedScore += 70;
                break;
            case 4:
                addedScore += 150;
                break;
            default:
                if (quantity <= 0)
                {
                    Debug.LogError("Value of quantity is less than or equal to zero: " + quantity);
                }
                else if (quantity >= 5)
                {
                    addedScore += 300 * (quantity - 4); //n-plicate * 300 
                }
                break;
        }
        score += addedScore;
        addedScoreText.text = "+" + addedScore.ToString();
        scoreAnim.SetTrigger(SHOW_TEXT);
        addToScore(); //Refresh score values.
        updateHighScore(score);
    }

    public void PerfectLevel(int hits)
    {
        string Msg;
        int Score;
        switch (hits)
        {
            case 0:
                Msg = "PERFECT! 100K";
                Score = 100000;
                break;
            case 1:
                Msg = "EXCELLENT! 50K";
                Score = 50000;
                break;
            case 2:
                Msg = "VERY GOOD! 20K";
                Score = 20000;
                break;
            case 3:
                Msg = "GOOD! 10K";
                Score = 10000;
                break;
            default:
                Msg = "NO BONUS...";
                Score = 0;
                break;
        }
        StartCoroutine(msgLevel(Msg, Score));
    }

    IEnumerator msgLevel(string Msg, int Score)
    {
        addToScore(Score); //add to score immediately
        yield return new WaitForSeconds(1.0f);
        if (Score != 0)
        {
            displayCurrentLevel(Msg);

        }
    }

    #region "Game Control Type"
    /// <summary>
    /// activate Swipe Mode. Invoked from button
    /// </summary>
    public void activateSwipe() { selectControlGameType(true); }
    /// <summary>
    /// Activate Two fingers Mode. Invoked from button
    /// </summary>
    public void activateTwoHandsInput() { selectControlGameType(false); }

    /// <summary>
    /// Select if the player is going to play with one finger (swipe=true) or two fingers (false)
    /// Recall Swipe and TwoFingers are different sides of same coin. Both can't be activated at same time, nor both deactivated.
    /// </summary>
    /// <param name="swipe"></param>
    private void selectControlGameType(bool swipe)
    {
        SwipeController sw = Rotator.GetComponent<SwipeController>();
        RotatorController rc = Rotator.GetComponent<RotatorController>();
        sw.enabled = swipe;
        rc.enabled = !swipe;

        //Switch button interface.
        SwipeBtn.gameObject.SetActive(swipe);
        TwoHandsBtn.gameObject.SetActive(!swipe);
        GPC.btnSwipe = swipe;
    }

    #endregion

}

/*Iteration 0: trying to simplify the code , since it embarrasing complex and difficult to handle
 * Current version hast 783 lines and many thing that should be simplified.
 * */
