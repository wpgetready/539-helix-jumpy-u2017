﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class TestCases {

	[Test]
	public void PowerUpLevelEmptyIfNotEnoughLevels() {
        // Use the Assert class to test conditions.
        List<int> results =PowerUp.definePowerUpLevels(24);
        Assert.AreEqual(0, results.Count);
    }

    [Test]
    public void PowerUpLevelReturnsAlwaysOne()
    {
        // Use the Assert class to test conditions.
        List<int> results = PowerUp.definePowerUpLevels(25);
        Debug.Log("Test 1:" + results[0]);
        Assert.AreEqual(1, results.Count);
    }


    [Test]
    public void PowerUpLevelReturnsAlwaysTwo()
    {
        // Use the Assert class to test conditions.
        List<int> results = PowerUp.definePowerUpLevels(50);
        Debug.Log("Test 1:" + results[0] + " " + results[1]);

        Assert.AreEqual(2, results.Count);
    }

    [Test]
    public void PowerUpLevelReturns4inAscendingOrder()
    {
        // Use the Assert class to test conditions.
        List<int> results = PowerUp.definePowerUpLevels(110);
        Debug.Log("Test 1:" + results[0] + " " + results[1]);

        Assert.AreEqual(2, results.Count);
    }


    /// <summary>
    /// Count all the elements inside a HelixController.
    /// </summary>
    [Test]
    public void CountNormalPieces()
    {
        //Made a folder for Testing: Resources/TestRunner/Prefabs
        //REMEMBER: Forward slashes (/) NOT backward
        GameObject g = Resources.Load<GameObject>("TestRunner/Prefabs/HelixTest"); // note: not .prefab!
        HelixController hc = g.GetComponent<HelixController>();
        //GameObject g = (GameObject)GameObject.Instantiate(pPrefab, Vector3.zero, Quaternion.identity); 
        Assert.AreEqual(12, HelixController.countNormalPieces(hc));
    }

    /// <summary>
    /// Load a prefab with 2 pieces Kill (tagged Finish) and 2 pieces invisible (MeshRenderer deactivated). It should Return 8
    /// </summary>
    [Test]
    public void Helix_with_2Kill_2_Disabled()
    {
        //Made a folder for Testing: Resources/TestRunner/Prefabs
        //REMEMBER: Forward slashes (/) NOT backward
        GameObject g = Resources.Load<GameObject>("TestRunner/Prefabs/HelixTest_2K_2D"); // note: not .prefab!
        HelixController hc = g.GetComponent<HelixController>();
        //GameObject g = (GameObject)GameObject.Instantiate(pPrefab, Vector3.zero, Quaternion.identity); 
        Assert.AreEqual(8, HelixController.countNormalPieces(hc));
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    //[UnityTest]
    public IEnumerator NewEditModeTestWithEnumeratorPasses() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}

	[Test]
	public void NewEditModeTest1SimplePasses() {
		// Use the Assert class to test conditions.
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode
	[UnityTest]
	public IEnumerator NewEditModeTest1WithEnumeratorPasses() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}
}
