﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOCommenter : MonoBehaviour {
	[TextArea(5,30)]
    public string Notes = "Comment Here."; // Do not place your note/comment here. 
                                           // Enter your note in the Unity Editor.
}
