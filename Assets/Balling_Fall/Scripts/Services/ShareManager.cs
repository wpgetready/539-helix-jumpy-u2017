﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

/*
 20191013: This game is the first iteration for making skineable Unity games
 In order to do that, all the parameters will need to be controlled outside, using the compile.cs file I've made.
 Currently this games has 3 variations: spinnerfall,hopdown and pylonball.
 So the first change is DISABLE appUrl being modifiable.
 */
namespace BallingFallGame
{
    public class ShareManager : MonoBehaviour
    {

        public static ShareManager Instance { get; private set; }

        [Header("Main App Configuration")]
        [SerializeField]
        private AppConfig appConfig;
       // [SerializeField] private AppConfig appConfig;

        public string ScreenshotPath { private set; get; }
        public string AppUrl { private set; get; }

        private void Awake()
        {
            if (Instance)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }

           // AppUrl = appUrl;
        }

        private void Start()
        {
             if (appConfig == null)
            {
                Debug.LogError("Error: AppConfig undefined on ShareManager.cs . Check ShareManager on scene");
            } else
            {
            }

        }

        public string formatDate()
        {
            DateTime now = DateTime.Now;
            return now.Year + string.Format("{0,2:D2}", now.Month) + string.Format("{0,2:D2}", now.Day)
                + string.Format("{0,2:D2}", now.Hour) + string.Format("{0,2:D2}", now.Minute) + string.Format("{0,2:D2}", now.Second);
        }

        /// <summary>
        /// Create the screenshot
        /// </summary>
        public void CreateScreenshot()
	    {
            CreateScreenshot("");
        }

        public void CreateScreenshot(string suffix)
        {
            string a = appConfig.ScreenShotName;
            string c = a;

            string[] b = a.Split('.');
            //Only works if there is one point 
            if (b.Length==2)
            {
                c = b[0] + "_" + suffix + "." + b[1];
            }

            ScreenshotPath = Path.Combine(Application.persistentDataPath, c);
#if UNITY_EDITOR
            ScreenCapture.CaptureScreenshot(ScreenshotPath);
#else
            ScreenCapture.CaptureScreenshot(appConfig.ScreenShotName);
#endif
        }

        public void CreateScreenshotWithPrefix()
        {
            CreateScreenshot("_" + formatDate());
        }

        /// <summary>
        /// Share screenshot with text
        /// </summary>
        public void NativeShare()
        {
            Share(appConfig.ShareText , ScreenshotPath, AppUrl, appConfig.ShareSubject );
        }


        /// <summary>
        /// Share on titter page
        /// </summary>
        public void TwitterShare()
        {
            Application.OpenURL(appConfig.TwitterAddress  + "?text=" + WWW.EscapeURL(appConfig.TwitterTextToDisplay ) + "&amp;lang=" + WWW.EscapeURL(appConfig.TwitterLanguage));
        }


        /// <summary>
        /// Share on facbook page
        /// </summary>
        public void FacebookShare()
        {
            if (!string.IsNullOrEmpty(appConfig.FbPictureUrl))
            {
                Application.OpenURL("https://www.facebook.com/dialog/feed?" + "app_id=" + appConfig.FbAppId + "&link=" + appConfig.AppUrl + "&picture=" + appConfig.FbPictureUrl
                             + "&caption=" + appConfig.FbSharedCaption + "&description=" + appConfig.FbDescription );
            }
            else
            {
                Application.OpenURL("https://www.facebook.com/dialog/feed?" + "app_id=" + appConfig.FbAppId  + "&link=" + appConfig.AppUrl + "&caption=" + appConfig.FbSharedCaption + "&description=" + appConfig.FbDescription);
            }
        }


        private void Share(string shareText, string imagePath, string url, string subject)
        {
#if UNITY_ANDROID
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            intentObject.Call<AndroidJavaObject>("setType", "image/png");

            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText + "  " + url);

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
            currentActivity.Call("startActivity", jChooser);
#elif UNITY_IOS

            CallSocialShareAdvanced(shareText, subject, url, imagePath);
#else
			Debug.Log("No sharing set up for this platform.");
#endif
        }


#if UNITY_IOS
    public struct ConfigStruct
    {
        public string title;
        public string message;
    }

    [DllImport("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

    public struct SocialSharingStruct
    {
        public string text;
        public string url;
        public string image;
        public string subject;
    }

    [DllImport("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

    public static void CallSocialShare(string title, string message)
    {
        ConfigStruct conf = new ConfigStruct();
        conf.title = title;
        conf.message = message;
        showAlertMessage(ref conf);
    }


    public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
    {
        SocialSharingStruct conf = new SocialSharingStruct();
        conf.text = defaultTxt;
        conf.url = url;
        conf.image = img;
        conf.subject = subject;

        showSocialSharing(ref conf);
    }
#endif
    }

}